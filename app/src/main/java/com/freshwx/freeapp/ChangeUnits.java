package com.freshwx.freeapp;

import java.io.Serializable;

public class ChangeUnits implements Serializable {

    public String temperature(String temperature, boolean fahrenheit) {
        double temp = Double.parseDouble(temperature)-273.15;
        if (fahrenheit) return String.valueOf(String.format("%.1f", (temp * 1.8) + 32.0));
        else return String.format("%.1f",temp);
    }

    public String temperatureNoDecimals(String temperature, boolean fahrenheit) {
        double temp = Double.parseDouble(temperature)-273.15;
        if (fahrenheit) return String.valueOf(Math.round(( temp * 1.8) + 32.0 ));
        else return String.valueOf(Math.round(temp));
    }

    public String celsiusToFahrenheit(String temperature, boolean needToChange){
        float temp = Float.parseFloat(temperature);
        if (needToChange) return String.valueOf(String.format("%.1f", (temp * 1.8) + 32.0));
        else return String.format("%.1f",temp);
    }

    public String celsiusToFahrenheitNoDecimals(String temperature, boolean needToChange){
        float temp = Float.parseFloat(temperature);
        if (needToChange) return String.valueOf(Math.round(( temp * 1.8) + 32.0 ));
        else return String.valueOf(Math.round(temp));
    }

    public String kphToMph(String speed, boolean needToChange){
        float spd = Float.parseFloat(speed);
        if(needToChange) return String.valueOf(String.format("%.1f", (float) 0.621371*spd));
        else return String.format("%.1f",spd);
    }

    public String hpaToAtm(String pressure, boolean needToChange){
        float prs = Float.parseFloat(pressure);
        if(needToChange) return String.valueOf(String.format("%.3f", prs/1013.25F));
        else return String.format("%.1f", prs);
    }

    public String mToYd(String distance, boolean needToChange){
        float dst = Float.parseFloat(distance);
        if(needToChange) return String.valueOf(Math.round(dst/0.9144F));
        else return distance;
    }

    public String kmToMi(String distance, boolean needToChange){
        float dst = Float.parseFloat(distance);
        if(needToChange) return String.valueOf(String.format("%.1f", dst*0.62137119F));
        else return String.format("%.1f", dst);
    }

}