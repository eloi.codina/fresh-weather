package com.freshwx.freeapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.freshwx.freeapp.objects.CurrentDataObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class Tools {

    Context context;
    Preferences preferences;

    public Tools(Context c) {
        context = c;
        preferences = new Preferences();
    }

    public ArrayList<Bitmap> getPreviewBitmapsArray() {

        ArrayList<Bitmap> bitmaps = new ArrayList<>();

        bitmaps.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.background_partcloud_prev));
        bitmaps.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.background_clear_prev));
        bitmaps.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.background_cloudy_prev));
        bitmaps.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.background_night_prev));
        bitmaps.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.background_rain_prev));

        return bitmaps;
    }

    public boolean isFollowing(CurrentDataObject dataObject) {
        /*
         * Returns true | false. Is the user already following the station?
         */
        String followedStations = preferences.loadString(context, "followedStations", null);
        if (followedStations == null) return false;
        return  followedStations.contains(dataObject.getStationID());
    }

    public boolean canFollow(CurrentDataObject dataObject) {
        /*
         * Returns whether the user is not following the station and it has not reached the limit
         * (max 4 OPWM stations)
         */
        String followedStations = preferences.loadString(context, "followedStations", null);
        if (followedStations == null) return true;
        boolean limitReached = followedStations.length() - followedStations.replace("O1PW_", "O1P_").length() >= 4;
        if (limitReached) {
            Toast.makeText(context, R.string.following_limit_reached, Toast.LENGTH_LONG).show();
            return false;
        }

        return !isFollowing(dataObject);
    }

    public void follow(CurrentDataObject dataObject) {
        String opwm_pre = dataObject.getOPWM() ? "O1PW_":"0_";
        try {
            String followedStations = preferences.loadString(context, "followedStations", null);
            if (followedStations == null) followedStations = "[]";

            JSONArray jsonArray = new JSONArray(followedStations);
            jsonArray.put(opwm_pre + dataObject.getStationID());

            followedStations = jsonArray.toString();
            preferences.saveString(context, "followedStations", followedStations);
        } catch (JSONException e) { }
    }

    public void unfollow(CurrentDataObject dataObject) {
        String opwm_pre = dataObject.getOPWM() ? "O1PW_":"0_";
        ArrayList<String> listFollowedStations = new ArrayList<>();
        try {
            String followedStations = preferences.loadString(context, "followedStations", null);
            if (followedStations == null) followedStations = "[]";
            if (!followedStations.contains(dataObject.getStationID())) return;

            JSONArray jsonArray = new JSONArray(followedStations);
            for (int i = 0; i<jsonArray.length(); i++){
                listFollowedStations.add(jsonArray.getString(i));
            }
            listFollowedStations.remove(opwm_pre + dataObject.getStationID());
            jsonArray = new JSONArray(listFollowedStations);

            followedStations = jsonArray.toString();
            preferences.saveString(context, "followedStations", followedStations);

        } catch (JSONException e) { }
    }
}
