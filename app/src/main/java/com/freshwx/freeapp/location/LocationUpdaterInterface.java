package com.freshwx.freeapp.location;


import android.location.Location;

import com.google.android.gms.common.api.Status;

public interface LocationUpdaterInterface {
    void locationUpdated(Location location);
    void enableLocation(Status status);
}

