package com.freshwx.freeapp.location;


import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;

import com.freshwx.freeapp.Preferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;


public class LocationUpdater extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    static int UPDATE_INTERVAL = 5000;     // 5.0 sec
    static int FASTEST_INTERVAL = 2000;    // 2.0 sec
    static int DISPLACEMENT = 100;         // 100 meters

    private GoogleApiClient mGoogleApiClient;

    final Preferences preferences = new Preferences();
    private final IBinder binder = new LocationBinder();
    private LocationUpdaterInterface locationInterface;
    PendingResult<LocationSettingsResult> result;


    @Override
    public void onCreate() {
        super.onCreate();

        int r = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if(r != 0){
            //System.out.println(r);
            //Dialog d = GoogleApiAvailability.getInstance().getErrorDialog(this,r,1);
            //d.show();
        }
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {


            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            //System.out.println("LAST LOCATION : " + Double.toString(mLastLocation.getLatitude()));

            if (mLastLocation != null && mLastLocation.getAccuracy() <= DISPLACEMENT) {
                updateLocation(mLastLocation);
                mGoogleApiClient.disconnect();
            } else {
                //you have to request for location
                LocationRequest mLocationRequest = LocationRequest.create();
                mLocationRequest.setInterval(UPDATE_INTERVAL);
                mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
                mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);




                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest);
                builder.setAlwaysShow(true);

                result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(@NonNull LocationSettingsResult result) {
                        final Status status = result.getStatus();
                        //final LocationSettingsStates state = result.getLocationSettingsStates();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                // All location settings are satisfied. The client can initialize location
                                // requests here.
                                //...
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                // Location settings are not satisfied. But could be fixed by showing the user
                                // a dialog.
                                if (locationInterface != null) {
                                    locationInterface.enableLocation(status);
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                // Location settings are not satisfied. However, we have no way to fix the
                                // settings so we won't show the dialog.
                                //...
                                break;
                        }
                    }
                });

                LocationServices.FusedLocationApi
                        .requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
    @Override
    public void onLocationChanged(Location location) {
        if (location.getAccuracy() <= DISPLACEMENT) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
            updateLocation(location);
        }

    }




    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        mGoogleApiClient.connect();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void updateLocation(Location location) {

        if (location != null) {
            preferences.saveString(this, "lastKnownLat", String.valueOf(location.getLatitude()));
            preferences.saveString(this, "lastKnownLon", String.valueOf(location.getLongitude()));

        } else {
            preferences.saveString(this, "lastKnownLat", "ERROR");
            preferences.saveString(this, "lastKnownLon", "ERROR");
        }
        preferences.saveString(this, "lastKnownLocationTime", String.valueOf(System.currentTimeMillis()));

        if (locationInterface != null) {
            locationInterface.locationUpdated(location);
        }

    }


    /* BINDER */

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void setCallbacks(LocationUpdaterInterface locInterface) {
        locationInterface = locInterface;
    }

    // Class used for the client Binder.
    public class LocationBinder extends Binder {
        public LocationUpdater getService() {
            // Return this instance of MyService so clients can call public methods
            return LocationUpdater.this;
        }
    }



}