package com.freshwx.freeapp.summary;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.helpSettings.HelpActivity;
import com.freshwx.freeapp.helpSettings.MyPreferencesActivity;
import com.freshwx.freeapp.internet.FetchClimaticInformation;
import com.freshwx.freeapp.internet.FetchCurrentData;
import com.freshwx.freeapp.internet.FetchLocation;
import com.freshwx.freeapp.internet.InternetConnection;
import com.freshwx.freeapp.location.LocationUpdater;
import com.freshwx.freeapp.location.LocationUpdaterInterface;
import com.freshwx.freeapp.objects.CurrentDataObject;
import com.freshwx.freeapp.objects.ExtraInfoObject;
import com.freshwx.freeapp.objects.LocationObject;
import com.freshwx.freeapp.search.ActivitySearch;
import com.freshwx.freeapp.showStation.AboutStation;
import com.freshwx.freeapp.showStation.ClimateInformation;
import com.freshwx.freeapp.showStation.ShowData;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.Random;


public class SummaryMain extends AppCompatActivity implements LocationUpdaterInterface {

    private final static int CLIMATE_ACTIVITY = 0;
    private final static int WEATHER_ACTIVITY = 1;
    private static final int PERMISSION_REQUEST_CODE = 1;

    SummaryUI ui;
    private LocationUpdater locationUpdater;
    private boolean locationUpdaterBound = false;
    InternetConnection internet = new InternetConnection();
    Preferences preferences = new Preferences();

    ArrayList<ExtraInfoObject> climaticInfo;
    CurrentDataObject currentData;
    LocationObject locationObject;

    Toolbar toolbar;

    String apiKey;



    @Override
    protected void onStart() {
        super.onStart();

        if (preferences.loadBoolean(this, "isFirstRun")) {
            preferences.deleteAllPreferences(this);
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.cookies))
                    .setMessage(getString(R.string.cookiesMessage))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            preferences.saveBoolean(getApplicationContext(), "isFirstRun", false);
                        }
                    }).setNeutralButton(getString(R.string.seeMore), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Uri uriUrl = Uri.parse("http://freshwx.com/privacypolicy");
                    Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                    startActivity(launchBrowser);
                }
            }).show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences.updateLanguage(this);

        setContentView(R.layout.smart_pres);


        toolbar = (Toolbar) findViewById(R.id.tool_bar_smart);
        setSupportActionBar(toolbar);

        ui = new SummaryUI(this);
        apiKey = preferences.loadString(getApplicationContext(), "api_key", "");

        try {
            Intent intent = getIntent();
            CurrentDataObject dataObject = intent.getParcelableExtra("currentData");
            LocationObject loc = intent.getParcelableExtra("location");

            if (dataObject == null || loc == null) {
                search();
            } else {
                locationObject = loc;
                currentData = dataObject;
                ui.setLocationCard(locationObject);
                ui.setWeatherCard(currentData);
                ui.visibilityRefreshing(SummaryUI.REFRESH_FRAME, SummaryUI.GONE);
                if (!currentData.getOPWM())
                    new FetchClimaticInformation(getApplicationContext(), new FetchClimaticInfoListener(),
                            currentData.getStationID(), apiKey).execute();
                else {
                    ui.refreshLayout.setRefreshing(false);
                    ui.hideClimateCard();
                }
            }
        } catch (Exception e) {
            search();
        }


        ui.fullReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), ShowData.class);
                intent.putExtra("currentData", currentData);
                intent.putParcelableArrayListExtra("climateInfo", climaticInfo);
                startActivityForResult(intent, WEATHER_ACTIVITY);
            }
        });

        ui.aboutStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentData.getOPWM()) {
                    showStationAboutDialog();
                } else {
                    preferences.saveString(getApplicationContext(), "LAST_STATION", currentData.getStationID());
                    Intent i = new Intent(getApplicationContext(), AboutStation.class);
                    startActivity(i);
                }
            }
        });

        ui.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ui.refreshLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (internet.isConnected(getApplicationContext())) {
                            search();
                        } else
                            Toast.makeText(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG).show();

                    }
                }, 1000);
            }
        });

        ui.climateMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), ClimateInformation.class);
                intent.putExtra("currentData", currentData);
                intent.putParcelableArrayListExtra("climateInfo", climaticInfo);
                startActivityForResult(intent, CLIMATE_ACTIVITY);
            }
        });

        ui.tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });


    }

    public void search() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_CODE);

            } else {
                if (internet.isConnected(this)) {
                    Intent intent = new Intent(this, LocationUpdater.class);
                    bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

                    ui.visibilityRefreshing(SummaryUI.CURRENT_TASK, SummaryUI.VISIBLE);
                    ui.visibilityRefreshing(SummaryUI.INFO_FOR_USER, SummaryUI.HIDE);
                    ui.visibilityRefreshing(SummaryUI.TRY_AGAIN, SummaryUI.HIDE);
                    ui.visibilityRefreshing(SummaryUI.PROGRESS_BAR, SummaryUI.VISIBLE);
                    ui.setRefreshingText(SummaryUI.CURRENT_TASK, getString(R.string.updatingLocation));
                } else {
                    ui.visibilityRefreshing(SummaryUI.CURRENT_TASK, SummaryUI.VISIBLE);
                    ui.visibilityRefreshing(SummaryUI.INFO_FOR_USER, SummaryUI.VISIBLE);
                    ui.visibilityRefreshing(SummaryUI.TRY_AGAIN, SummaryUI.VISIBLE);
                    ui.visibilityRefreshing(SummaryUI.PROGRESS_BAR, SummaryUI.HIDE);
                    ui.setRefreshingText(SummaryUI.CURRENT_TASK, getString(R.string.noConnection));
                    ui.setRefreshingText(SummaryUI.INFO_FOR_USER, getString(R.string.tryLater));
                }
            }
        } else {
            if (internet.isConnected(this)) {
                Intent intent = new Intent(this, LocationUpdater.class);
                bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

                ui.visibilityRefreshing(SummaryUI.CURRENT_TASK, SummaryUI.VISIBLE);
                ui.visibilityRefreshing(SummaryUI.INFO_FOR_USER, SummaryUI.HIDE);
                ui.visibilityRefreshing(SummaryUI.TRY_AGAIN, SummaryUI.HIDE);
                ui.visibilityRefreshing(SummaryUI.PROGRESS_BAR, SummaryUI.VISIBLE);
                ui.setRefreshingText(SummaryUI.CURRENT_TASK, getString(R.string.updatingLocation));
            } else {
                ui.visibilityRefreshing(SummaryUI.CURRENT_TASK, SummaryUI.VISIBLE);
                ui.visibilityRefreshing(SummaryUI.INFO_FOR_USER, SummaryUI.VISIBLE);
                ui.visibilityRefreshing(SummaryUI.TRY_AGAIN, SummaryUI.VISIBLE);
                ui.visibilityRefreshing(SummaryUI.PROGRESS_BAR, SummaryUI.HIDE);
                ui.setRefreshingText(SummaryUI.CURRENT_TASK, getString(R.string.noConnection));
                ui.setRefreshingText(SummaryUI.INFO_FOR_USER, getString(R.string.tryLater));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, LocationUpdater.class);
                    bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
                } else {
                    Toast.makeText(this, R.string.needToGrantPermission, Toast.LENGTH_LONG).show();
                    System.exit(0);
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent i = new Intent(this, MyPreferencesActivity.class);
            startActivity(i);
            return true;
        } else if (id == R.id.action_about) {
            Intent i = new Intent(this, HelpActivity.class);
            startActivity(i);
            return true;
        } else if (id == R.id.search) {
            Intent i = new Intent(this, ActivitySearch.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void showStationAboutDialog() {
        new AlertDialog.Builder(this)
                .setTitle(currentData.getStationName())
                .setMessage(getString(R.string.about_opwm))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).setNeutralButton(getString(R.string.visitWebPage), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Uri uriUrl = Uri.parse("http://openweathermap.org/");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        }).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (CLIMATE_ACTIVITY) :
                if (resultCode == Activity.RESULT_OK) {
                    climaticInfo.clear();
                    currentData = data.getParcelableExtra("currentData");
                    climaticInfo = data.getParcelableArrayListExtra("climateInfo");
                    Random random = new Random();
                    ui.setClimateInformation(climaticInfo.get(random.nextInt(climaticInfo.size())));
                    ui.visibilityRefreshing(SummaryUI.REFRESH_FRAME, SummaryUI.GONE);
                }
                break;
            case (WEATHER_ACTIVITY):
                if (resultCode == Activity.RESULT_OK) {
                    currentData = data.getParcelableExtra("currentData");
                    climaticInfo = data.getParcelableArrayListExtra("climateInfo");
                    ui.setWeatherCard(currentData);
                    ui.visibilityRefreshing(SummaryUI.REFRESH_FRAME, SummaryUI.GONE);
                }
                break;
            case 199:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        search();
                        break;

                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, R.string.location_not_enabled, Toast.LENGTH_LONG).show();
                        break;

                    default:

                        break;

                }
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        disconnectFromLocationService();
    }







    public class FetchLocationListener implements AsyncTaskCompleteListener<LocationObject> {

        @Override
        public void onTaskComplete(LocationObject result) {
            locationObject = result;

            ui.setLocationCard(locationObject);

            new FetchCurrentData(getApplicationContext(), new FetchCurrentDataListener(), FetchCurrentData.NEARBY,
                    Double.toString(locationObject.getLat()), Double.toString(locationObject.getLon()),
                    null, null, null, null, apiKey).execute();
        }
    }

    public class FetchCurrentDataListener implements AsyncTaskCompleteListener<ArrayList<CurrentDataObject>> {

        @Override
        public void onTaskComplete(ArrayList<CurrentDataObject> result) {
            if (result.size() >= 1) {
                currentData = result.get(0);
                ui.setWeatherCard(currentData);
                if (!currentData.getOPWM())
                    new FetchClimaticInformation(getApplicationContext(), new FetchClimaticInfoListener(),
                            currentData.getStationID(), apiKey).execute();
                else {
                    ui.refreshLayout.setRefreshing(false);
                    ui.hideClimateCard();
                    ui.visibilityRefreshing(SummaryUI.REFRESH_FRAME, SummaryUI.GONE);
                }

            } else {
                ui.refreshLayout.setRefreshing(false);
                ui.refreshLayout.setEnabled(false);
                ui.setRefreshingText(SummaryUI.INFO_FOR_USER, getString(R.string.tryLater));
                ui.setRefreshingText(SummaryUI.CURRENT_TASK, getString(R.string.experiencingProblems));
                ui.visibilityRefreshing(SummaryUI.PROGRESS_BAR, SummaryUI.GONE);
                ui.visibilityRefreshing(SummaryUI.TRY_AGAIN, SummaryUI.VISIBLE);
            }
        }
    }

    public class FetchClimaticInfoListener implements AsyncTaskCompleteListener<ArrayList<ExtraInfoObject>> {

        @Override
        public void onTaskComplete(final ArrayList<ExtraInfoObject> result) {
            climaticInfo = result;
            if (result.size() == 0) ui.hideClimateCard();
            else {
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        Random random = new Random();
                        final ExtraInfoObject info = climaticInfo.get(random.nextInt(result.size()));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ui.setClimateInformation(info);
                            }
                        });
                    }
                };
                t.start();
                ui.refreshLayout.setRefreshing(false);
            }
            ui.visibilityRefreshing(SummaryUI.REFRESH_FRAME, SummaryUI.GONE);
        }
    }




    /* LOCATION */
    private void disconnectFromLocationService() {
        if (locationUpdaterBound) {
            locationUpdater.setCallbacks(null); // unregister
            unbindService(serviceConnection);
            locationUpdaterBound = false;
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // cast the IBinder and get MyService instance
            LocationUpdater.LocationBinder binder = (LocationUpdater.LocationBinder) service;
            locationUpdater = binder.getService();
            locationUpdater.setCallbacks(SummaryMain.this); // register
            locationUpdaterBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            locationUpdaterBound = false;
        }
    };

    /* Defined by ServiceCallbacks interface */
    @Override
    public void locationUpdated(Location location) {
        ui.setRefreshingText(SummaryUI.CURRENT_TASK, getString(R.string.downloadingInformation));
        new FetchLocation(new FetchLocationListener(), location.getLatitude(), location.getLongitude(), getApplicationContext(), apiKey).execute();
        disconnectFromLocationService();
    }

    @Override
    public void enableLocation(Status status) {
        try {
            // Show the dialog by calling startResolutionForResult(),
            // and check the result in onActivityResult().
            status.startResolutionForResult(
                    SummaryMain.this,
                    199);
        } catch (IntentSender.SendIntentException e) {
            // Ignore the error.
        }

        disconnectFromLocationService();
    }
}
