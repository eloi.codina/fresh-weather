package com.freshwx.freeapp.summary;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.freshwx.freeapp.ChangeUnits;
import com.freshwx.freeapp.CorrectTexts;
import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.Tools;
import com.freshwx.freeapp.objects.CurrentDataObject;
import com.freshwx.freeapp.objects.ExtraInfoObject;
import com.freshwx.freeapp.objects.LocationObject;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Locale;


public class SummaryUI {

    Activity act;
    Preferences preferences;
    ChangeUnits changeUnits;
    Tools tools;

    ImageButton toggleFollow, openMap, aboutStation;
    TextView loc1, loc2, coord;
    Button fullReport;

    ImageView weatherBackground;
    TextView temperature, temperatureUnits, pressure, humid, wind, skyDescription, stationCity, opwm;

    TextView climateExample;
    Button climateMore;

    CardView locationCard, weatherCard, climateCard;

    ProgressBar progressBar;
    TextView infoForUser, currentTask;
    ImageButton tryAgain;
    FrameLayout frameRefresh;

    SwipeRefreshLayout refreshLayout;

    public final static int INFO_FOR_USER = 0;
    public final static int CURRENT_TASK = 1;
    public final static int PROGRESS_BAR = 2;
    public final static int TRY_AGAIN = 3;
    public final static int REFRESH_FRAME = 4;

    public final static int HIDE = View.INVISIBLE;
    public final static int VISIBLE = View.VISIBLE;
    public final static int GONE = View.GONE;



    SummaryUI(Activity activity) {
        this.act = activity;
        preferences = new Preferences();
        changeUnits = new ChangeUnits();
        tools = new Tools(activity);

        getViews();
    }


    public void getViews() {
        refreshLayout = (SwipeRefreshLayout) act.findViewById(R.id.smart_swipe_refresh_layout);

        // Location card
        locationCard = (CardView) act.findViewById(R.id.card_view_location);
        toggleFollow = (ImageButton) act.findViewById(R.id.location_toggle_follow);
        openMap = (ImageButton) act.findViewById(R.id.location_open_map);
        loc1 = (TextView) act.findViewById(R.id.loc1);
        loc2 = (TextView) act.findViewById(R.id.loc2);
        coord = (TextView) act.findViewById(R.id.coord);
        aboutStation = (ImageButton) act.findViewById(R.id.location_information);

        // Weather card
        weatherCard = (CardView) act.findViewById(R.id.smart_cardView);
        weatherBackground = (ImageView) act.findViewById(R.id.smart_data_background);
        temperature = (TextView) act.findViewById(R.id.smart_data_temperature);
        temperatureUnits = (TextView) act.findViewById(R.id.smart_data_units);
        pressure = (TextView) act.findViewById(R.id.smart_data_pressure);
        humid = (TextView) act.findViewById(R.id.smart_data_humidity);
        wind = (TextView) act.findViewById(R.id.smart_data_wind);
        skyDescription = (TextView) act.findViewById(R.id.smart_data_sky_description);
        opwm = (TextView) act.findViewById(R.id.smart_data_opwm);
        stationCity = (TextView) act.findViewById(R.id.smart_data_city);
        fullReport = (Button) act.findViewById(R.id.smart_data_full_report);


        // Information card
        climateCard = (CardView) act.findViewById(R.id.climate_cardView);
        climateExample = (TextView) act.findViewById(R.id.climate_example);
        climateMore = (Button) act.findViewById(R.id.climate_more);

        //Refreshing frame layout
        progressBar = (ProgressBar) act.findViewById(R.id.smart_progressBar);
        infoForUser = (TextView) act.findViewById(R.id.smart_info_for_user);
        currentTask = (TextView) act.findViewById(R.id.smart_current_task_loading);
        tryAgain = (ImageButton) act.findViewById(R.id.smart_try_again);
        frameRefresh = (FrameLayout) act.findViewById(R.id.smart_refreshing_layout);

        new Thread() {
            public void run() {
                try {
                    sleep(1000);
                    act.runOnUiThread(new Runnable() {
                        public void run() {
                            AdView mAdView = (AdView) act.findViewById(R.id.smartAdView);
                            AdRequest adRequest = new AdRequest.Builder().build();
                            mAdView.loadAd(adRequest);
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }

    public void setRefreshingText(int element, String text) {
        switch (element) {
            case INFO_FOR_USER:
                infoForUser.setText(text);
                break;
            case CURRENT_TASK:
                currentTask.setText(text);
        }
    }

    public void visibilityRefreshing(int element, int state) {
        switch (element) {
            case INFO_FOR_USER:
                infoForUser.setVisibility(state);
                break;
            case CURRENT_TASK:
                currentTask.setVisibility(state);
                break;
            case PROGRESS_BAR:
                progressBar.setVisibility(state);
                break;
            case TRY_AGAIN:
                tryAgain.setVisibility(state);
                break;
            case REFRESH_FRAME:
                frameRefresh.setVisibility(View.GONE);

        }
    }


    public void setLocationCard(final LocationObject locationObject) {
        loc1.setText(locationObject.getCity());
        loc2.setText(locationObject.getPlace());
        coord.setText(locationObject.getLatLon());

        locationCard.setVisibility(View.VISIBLE);

        openMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", locationObject.getLat(), locationObject.getLon());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                act.startActivity(intent);
            }
        });
    }

    public void setWeatherCard(final CurrentDataObject data) {
        stationCity.setText(data.getStationName());

        Thread setImage = new Thread() {
            @Override
            public void run() {
                final Bitmap weather = data.getPreviewBitmap(act);
                act.runOnUiThread(new Runnable() {
                    public void run() { weatherBackground.setImageBitmap(weather); }
                });
            }
        };
        setImage.start();

        Thread setText = new Thread() {
            @Override
            public void run() {
                final boolean celsius = preferences.loadString(act, "temperatureUnits", "C").equals("C");
                act.runOnUiThread(new Runnable() {
                    public void run() {
                        temperature.setText(data.getTempCurrentNoDecimals(act, false));
                        skyDescription.setText(data.getSkyConditionText(act));
                        pressure.setText(data.getPressure(act, true));
                        wind.setText(data.getWindSpeed(act, true));
                        humid.setText(data.getHumidity(act, true));

                        if(data.getOPWM()) opwm.setVisibility(View.VISIBLE);
                        else opwm.setVisibility(View.INVISIBLE);

                        if(!celsius) temperatureUnits.setText(R.string.degreesF);
                        else temperatureUnits.setText(R.string.degreesC);

                        weatherCard.setVisibility(View.VISIBLE);
                    }
                });
            }
        };
        setText.start();

        aboutStation.setVisibility(View.VISIBLE);
        new InitializeFollowStation(toggleFollow, data).execute();
        toggleFollow.setVisibility(View.VISIBLE);

        toggleFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ToggleFollowStation(toggleFollow, data).execute();
            }
        });
    }

    public void setClimateInformation(ExtraInfoObject data) {
        CorrectTexts correctTexts = new CorrectTexts();
        boolean celsius = preferences.loadString(act, "temperatureUnits", "C").equals("C");

        climateExample.setText(correctTexts.findReplaceTemperatures(data.getmText(), !celsius));
        climateCard.setVisibility(View.VISIBLE);
    }

    public void hideWeatherCard() {
        weatherCard.setVisibility(View.GONE);
    }

    public void hideClimateCard() {
        climateCard.setVisibility(View.GONE);
    }




    private class InitializeFollowStation extends AsyncTask<String, String, Integer> {

        ImageButton follow;
        CurrentDataObject data;

        public InitializeFollowStation(ImageButton follow, CurrentDataObject dataObject) {
            this.follow = follow;
            data = dataObject;
        }

        @Override
        protected void onPreExecute() { super.onPreExecute(); }

        @Override
        protected Integer doInBackground(String... args) {
            int image;
            if (tools.isFollowing(data))
                image = act.getResources().getIdentifier(String.valueOf(R.drawable.following), null, act.getString(R.string.packageName));
            else
                image = act.getResources().getIdentifier(String.valueOf(R.drawable.not_following), null, act.getString(R.string.packageName));


            return image;
        }
        @Override
        protected void onPostExecute(Integer image) {
            follow.setImageDrawable(ContextCompat.getDrawable(act, image));
        }
    }

    private class ToggleFollowStation extends AsyncTask<String, String, Integer> {

        CurrentDataObject data;
        ImageButton follow;

        ToggleFollowStation(ImageButton follow, CurrentDataObject dataObject) {
            this.follow = follow;
            data = dataObject;
        }

        @Override
        protected void onPreExecute() { super.onPreExecute(); }

        @Override
        protected Integer doInBackground(String... args) {
            int image;
            if (tools.canFollow(data)) {
                tools.follow(data);
                image = act.getResources().getIdentifier(String.valueOf(R.drawable.following), null, act.getString(R.string.packageName));
            } else {
                tools.unfollow(data);
                image = act.getResources().getIdentifier(String.valueOf(R.drawable.not_following), null, act.getString(R.string.packageName));
            }
            return image;
        }
        @Override
        protected void onPostExecute(Integer image) {
            follow.setImageDrawable(ContextCompat.getDrawable(act, image));
        }
    }
}
