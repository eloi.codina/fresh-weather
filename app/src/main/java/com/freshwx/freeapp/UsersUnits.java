package com.freshwx.freeapp;

import android.content.Context;

import java.io.Serializable;


public class UsersUnits implements Serializable {

    Context c;
    final Preferences preferences = new Preferences();

    public UsersUnits(Context context) {
        c = context;
    }

    public boolean celsius() {
        return preferences.loadString(c, "temperatureUnits", "C").equals("C");
    }

    public boolean kph() {
        return preferences.loadString(c, "windUnits", "k").equals("k");
    }

    public boolean hpa() {
        return preferences.loadString(c, "pressureUnits", "h").equals("h");
    }

    public String temperatureUnits() {
        return celsius() ? "ºC" : "ºF";
    }

    public String windUnits() {
        return kph() ? "km/h" : "mph";
    }

    public String pressureUnits() {
        return hpa() ? "hPa" : "atm";
    }

}
