package com.freshwx.freeapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;


public class Tutorial extends AppIntro2 {

    int slide = 1;
    int color1 = Color.parseColor("#afb42b");
    int color2 = Color.parseColor("#00796b");
    int color3 = Color.parseColor("#7b1fa2");
    int color4 = Color.parseColor("#ffa000");
    int color5 = Color.parseColor("#5d4037");

    // Please DO NOT override onCreate. Use init.
    @Override
    public void init(Bundle savedInstanceState) {

        addSlide(AppIntroFragment.newInstance(getString(R.string.tutorial_welcome_title), getString(R.string.tutorial_welcome_text), R.drawable.logo, color1));

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                addSlide(AppIntroFragment.newInstance(getString(R.string.tutorial_weather_title), getString(R.string.tutorial_weather_text) + getString(R.string.tutorial_weather_locate_text), R.drawable.location, color2));
                askForPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
            } else {
                addSlide(AppIntroFragment.newInstance(getString(R.string.tutorial_weather_title), getString(R.string.tutorial_weather_text), R.drawable.partlycloudy, color2));
            }
        } else {
            addSlide(AppIntroFragment.newInstance(getString(R.string.tutorial_weather_title), getString(R.string.tutorial_weather_text), R.drawable.partlycloudy, color2));
        }

        addSlide(AppIntroFragment.newInstance(getString(R.string.tutorial_climate_title), getString(R.string.tutorial_climate_text), R.drawable.chart, color3));
        addSlide(AppIntroFragment.newInstance(getString(R.string.tutorial_search_title), getString(R.string.tutorial_search_text), R.drawable.umbrella, color4));
        addSlide(AppIntroFragment.newInstance(getString(R.string.tutorial_finish_title), getString(R.string.tutorial_finish_text), R.drawable.tick,color5));

        showStatusBar(false);
    }


    @Override
    public void onNextPressed() {
        slide += 1;
    }

    @Override
    public void onDonePressed() {
        this.finish();
    }

    @Override
    public void onSlideChanged() {    }

}

