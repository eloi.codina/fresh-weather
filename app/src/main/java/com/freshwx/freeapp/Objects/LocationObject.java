package com.freshwx.freeapp.objects;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

public class LocationObject implements Parcelable {

    private String city, place;
    private double lat, lon;
    private long timeCreated;

    public LocationObject(@Nullable String city, @Nullable String place, double lat, double lon, long timeCreated){
        this.city = city;
        this.place = place;
        this.lat = lat;
        this.lon = lon;
        this.timeCreated = timeCreated;
    }

    public String getCity() {
        return this.city;
    }

    public String getPlace() {
        return this.place;
    }

    public double getLat() {
        return this.lat;
    }

    public double getLon() {
        return this.lon;
    }

    public String getLatLon() {
        return String.format("%.2f", lat) + ", " + String.format("%.2f", lon);
    }

    public long getTimeCreated() {
        return this.timeCreated;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(city);
        out.writeString(place);
        out.writeDouble(lat);
        out.writeDouble(lon);
        out.writeLong(timeCreated);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<LocationObject> CREATOR = new Parcelable.Creator<LocationObject>() {
        public LocationObject createFromParcel(Parcel in) {
            return new LocationObject(in);
        }

        public LocationObject[] newArray(int size) {
            return new LocationObject[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private LocationObject(Parcel in) {
        city = in.readString();
        place = in.readString();
        lat = in.readDouble();
        lon = in.readDouble();
        timeCreated = in.readLong();
    }
}
