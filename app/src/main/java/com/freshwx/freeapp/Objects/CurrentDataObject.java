package com.freshwx.freeapp.objects;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.freshwx.freeapp.ChangeUnits;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.UsersUnits;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class CurrentDataObject implements Parcelable {

    private String mStationID;
    private String mStationName;
    private double mTimeCreated;
    private double mTempMax;
    private double mTempMin;
    private double mTempCurrent;
    private double mPressure;
    private int mHumidity;
    private double mWindSpeed;
    private double mWindHighSpeed;
    private String mWindDirection;
    private long mSunset;
    private long mSunrise;
    private double mRain;
    private int mSolarRadiation;
    private double mUVIndex;
    private long stationDateTime;
    private String mTemperatureTendency;
    private String mSky;
    private boolean mOPWM;

    ChangeUnits changeUnits;



    public CurrentDataObject (String stationID, String stationName, double timeCreated, double tempMax, double tempMin,
                       double tempCurrent, double pressure, int humidity, double windSpeed,
                       double windHighSpeed, String windDirection, long sunset, long sunrise,
                       double rain, int solarRadiation, double uvIndex, long datetime,
                       String temperatureTendency, String sky, boolean opwm){

        changeUnits = new ChangeUnits();

        mStationID = stationID;
        mStationName = stationName;
        mTimeCreated = timeCreated;
        mTempMax = tempMax;
        mTempMin = tempMin;
        mTempCurrent = tempCurrent;
        mPressure = pressure;
        mHumidity = humidity;
        mWindSpeed = windSpeed;
        mWindHighSpeed = windHighSpeed;
        mWindDirection = windDirection;
        mSunset = sunset;
        mSunrise = sunrise;
        mRain = rain;
        mSolarRadiation = solarRadiation;
        mUVIndex = uvIndex;
        stationDateTime = datetime;
        mTemperatureTendency = temperatureTendency;
        mSky = sky;
        mOPWM = opwm;
    }

    public String getStationID() { return mStationID; }

    public void setStationID(String mText) { this.mStationID = mText; }

    public String getStationName() { return mStationName; }

    public void setStationName(String mText) { this.mStationName = mText; }


    public double getTimeCreated() { return mTimeCreated; }

    public void setTimeCreated(double mText) { this.mTimeCreated = mText; }


    public String getTempMax(Context context, boolean units) {
        if (mTempMax > -999) {
            String tempMax = Double.toString(mTempMax);
            if (!units)
                return changeUnits.temperature(tempMax, !(new UsersUnits(context).celsius()));

            UsersUnits usersUnits = new UsersUnits(context);
            return changeUnits.temperature(tempMax, !usersUnits.celsius()) + " " + usersUnits.temperatureUnits();
        }
        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setTempMax(float mText) { this.mTempMax = mText; }


    public String getTempMin(Context context, boolean units) {
        if (mTempMin > -999) {
            String tempMin = Double.toString(mTempMin);
            if (!units)
                return changeUnits.temperature(tempMin, !(new UsersUnits(context).celsius()));

            UsersUnits usersUnits = new UsersUnits(context);
            return changeUnits.temperature(tempMin, !usersUnits.celsius()) + " " + usersUnits.temperatureUnits();
        }

        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setTempMin(long mText) { this.mTempMin = mText; }


    public String getTempCurrent(Context context, boolean units) {
        if (mTempCurrent > -999) {
            String tempCurrent = Double.toString(mTempCurrent);
            if (!units)
                return changeUnits.temperature(tempCurrent, !(new UsersUnits(context).celsius()));

            UsersUnits usersUnits = new UsersUnits(context);
            return changeUnits.temperature(tempCurrent, !usersUnits.celsius()) + " " + usersUnits.temperatureUnits();
        }

        return context.getResources().getString(R.string.nonAvailable);
    }

    public String getTempCurrentNoDecimals(Context context, boolean units) {
        if (mTempCurrent > -999) {
            String tempCurrent = Double.toString(mTempCurrent);
            if (!units)
                return changeUnits.temperatureNoDecimals(tempCurrent, !(new UsersUnits(context).celsius()));

            UsersUnits usersUnits = new UsersUnits(context);
            return changeUnits.temperatureNoDecimals(tempCurrent, !usersUnits.celsius()) + " " + usersUnits.temperatureUnits();
        }

        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setTempCurrent(float mText) { this.mTempCurrent = mText; }


    public String getPressure(Context context, boolean units) {
        if (mPressure > -999) {
            String pressure = Double.toString(mPressure);
            if (!units)
                return changeUnits.hpaToAtm(pressure, !(new UsersUnits(context).hpa()));

            UsersUnits usersUnits = new UsersUnits(context);
            return changeUnits.hpaToAtm(pressure, !(usersUnits.hpa())) + " " + usersUnits.pressureUnits();
        }
        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setPressure(long mText) { this.mPressure = mText; }


    public String getHumidity(Context context, boolean units) {
        if (mHumidity > -999) {
            String humidity = Integer.toString(mHumidity);
            return units ? humidity + "%" : humidity;
        }
        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setHumidity(int mText) { this.mHumidity = mText; }


    public String getWindSpeed(Context context, boolean units) {
        if (mWindSpeed > -999) {
            String windSpeed = Double.toString(mWindSpeed);
            if (!units)
                return changeUnits.kphToMph(windSpeed, !(new UsersUnits(context).kph()));

            UsersUnits usersUnits = new UsersUnits(context);
            return changeUnits.kphToMph(windSpeed, !(usersUnits.kph())) + " " + usersUnits.windUnits();
        }
        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setWindSpeed(long mText) { this.mWindSpeed = mText; }


    public String getWindHighSpeed(Context context, boolean units) {
        if (mWindHighSpeed > -999) {
            String windHighSpeed = Double.toString(mWindHighSpeed);
            if (!units)
                return changeUnits.kphToMph(windHighSpeed, !(new UsersUnits(context).kph()));

            UsersUnits usersUnits = new UsersUnits(context);
            return changeUnits.kphToMph(windHighSpeed, !(usersUnits.kph())) + " " + usersUnits.windUnits();
        }
        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setWindHighSpeed(long mText) { this.mWindHighSpeed = mText; }


    public String getWindDirection(Context context) {
        if (!Character.isDigit(mWindDirection.charAt(0)) && mWindDirection.charAt(0) != '-') {
            return mWindDirection;
        } else if (mWindDirection.equals("-999") || mWindDirection.equals("-999.9")) {
            return context.getResources().getString(R.string.nonAvailable);
        }

        String[] directions = {"N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W",
                "WNW","NW","NNW","N"};
        Float wd = Float.parseFloat(mWindDirection);

        return directions[ (int) Math.round((wd % 360)/22.5) ];
    }

    public void setWindDirection(String mText) { this.mWindDirection = mText; }


    public String getSunrise() {
        DateFormat f = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date time = new Date();
        time.setTime(mSunrise*1000);
        return f.format(time) + " UTC";
    }

    public void setSunrise(long timestamp) { this.mSunrise = timestamp; }


    public String getSunset() {
        DateFormat f = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date time = new Date();
        time.setTime(mSunset*1000);
        return f.format(time) + " UTC";
    }

    public void setmSunset(long timestamp) { this.mSunset = timestamp; }


    public String getRain(Context context, boolean units) {
        if (mRain > -999) {
            String rain = Double.toString(mRain);
            return units ? rain + " mm" : rain;
        }
        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setRain(long mText) { this.mRain = mText; }


    public String getSolarRadiation(Context context, boolean units) {
        if (mSolarRadiation > -999) {
            String solarRadiation = Integer.toString(mSolarRadiation);
            return !units ? solarRadiation : solarRadiation + " " + context.getString(R.string.solarRadiationUnits);
        }
        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setSolarRadiation(int mText) { this.mSolarRadiation = mText; }

    public String getUVIndex(Context context) {
        if (mUVIndex > -999)
            return Double.toString(mUVIndex);
        return context.getResources().getString(R.string.nonAvailable);
    }

    public void setUVIndex(long mText) { this.mUVIndex = mText; }


    public String getTimeStation(Activity act) {
        DateFormat f = DateFormat.getTimeInstance(DateFormat.SHORT, act.getResources().getConfiguration().locale);
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date time = new Date();
        time.setTime(stationDateTime*1000);
        return f.format(time);
    }

    public String getDateStation(Activity act) {
        DateFormat f = DateFormat.getDateInstance(DateFormat.SHORT, act.getResources().getConfiguration().locale);
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        date.setTime(stationDateTime*1000);
        return f.format(date);
    }

    public void setDateTimeStation(long timestamp) { this.stationDateTime = timestamp; }

    public boolean getOPWM() { return mOPWM; }

    public void setOPWM(boolean opwm) { mOPWM = opwm; }


    public int getTemperatureTendency(Context context) {
        String ubi;
        switch (mTemperatureTendency){
            case "UP":
                ubi = "@drawable/arrowup";
                break;
            case "DOWN":
                ubi = "@drawable/arrowdown";
                break;
            case "EQUAL":
                ubi = "@drawable/equal";
                break;
            default:
                ubi = "@drawable/equal";
        }

        return context.getResources().getIdentifier(ubi, null, context.getString(R.string.packageName));
    }

    public void setTemperatureTendency(String mText) { this.mTemperatureTendency = mText; }

    public int getWhiteSkyIcon(Context context) {
        String ubi;
        switch (mSky) {
            case "CLEAR":
                ubi = "@drawable/white_sun";
                break;
            case "PARTCLOUD":
                ubi = "@drawable/white_partlycloudy";
                break;
            case "RAIN":
                ubi = "@drawable/white_rainy";
                break;
            case "CLOUD":
                ubi = "@drawable/white_cloudy";
                break;
            case "NIGHT":
                ubi ="@drawable/white_night";
                break;
            default:
                ubi = "@drawable/white_partlycloudy";
        }
        return context.getResources().getIdentifier(ubi, null, context.getString(R.string.packageName));
    }

    public void setSkyCondition(String mText) { this.mSky = mText; }

    public int getColorSkyIcon(Context context) {
        String ubi;
        switch (mSky) {
            case "CLEAR":
                ubi = "@drawable/sunny";
                break;
            case "PARTCLOUD":
                ubi = "@drawable/partlycloudy";
                break;
            case "RAIN":
                ubi = "@drawable/rainy";
                break;
            case "CLOUD":
                ubi = "@drawable/cloudy";
                break;
            case "NIGHT":
                ubi ="@drawable/night";
                break;
            default:
                ubi = "@drawable/partlycloudy";
        }
        return context.getResources().getIdentifier(ubi, null, context.getString(R.string.packageName));
    }

    public String getSkyConditionText(Context c) {
        switch (mSky) {
            case "CLEAR":
                return c.getString(R.string.skyClear);
            case "PARTCLOUD":
                return c.getString(R.string.skyPartlyCloudy);
            case "RAIN":
                return  c.getString(R.string.skyRaining);
            case "CLOUD":
                return c.getString(R.string.skyCloudy);
            case "NIGHT":
                return c.getString(R.string.skyNight);
            default:
                return c.getString(R.string.skyPartlyCloudy);
        }
    }

    public Drawable getBackgroundDrawable(Context context) {
        int ubi;
        switch (mSky) {
            case "CLEAR":
                ubi = R.drawable.background_clear;
                break;
            case "PARTCLOUD":
                ubi = R.drawable.background_partcloud;
                break;
            case "RAIN":
                ubi =  R.drawable.background_rain;
                break;
            case "CLOUD":
                ubi = R.drawable.background_cloudy;
                break;
            case "NIGHT":
                ubi = R.drawable.background_night;
                break;
            default:
                ubi = R.drawable.background_partcloud;
        }

        return ContextCompat.getDrawable(context, ubi);
    }

    public Bitmap getPreviewBitmap(Context context) {
        int ubi;
        switch (mSky) {
            case "CLEAR":
                ubi = R.drawable.background_clear_prev;
                break;
            case "PARTCLOUD":
                ubi = R.drawable.background_partcloud_prev;
                break;
            case "RAIN":
                ubi =  R.drawable.background_rain_prev;
                break;
            case "CLOUD":
                ubi = R.drawable.background_cloudy_prev;
                break;
            case "NIGHT":
                ubi = R.drawable.background_night_prev;
                break;
            default:
                ubi = R.drawable.background_partcloud_prev;
        }

        return BitmapFactory.decodeResource(context.getResources(), ubi);
    }

    public int getPreviewBitmapPosition() {
        switch (mSky) {
            case "PARTCLOUD":
                return 0;
            case "CLEAR":
                return 1;
            case "CLOUD":
                return 2;
            case "NIGHT":
                return 3;
            case "RAIN":
                return 4;
            default:
                return 0;
        }
    }






    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mStationID);
        out.writeString(mStationName);
        out.writeDouble(mTimeCreated);
        out.writeDouble(mTempMax);
        out.writeDouble(mTempMin);
        out.writeDouble(mTempCurrent);
        out.writeDouble(mPressure);
        out.writeInt(mHumidity);
        out.writeDouble(mWindSpeed);
        out.writeDouble(mWindHighSpeed);
        out.writeString(mWindDirection);
        out.writeLong(mSunset);
        out.writeLong(mSunrise);
        out.writeDouble(mRain);
        out.writeInt(mSolarRadiation);
        out.writeDouble(mUVIndex);
        out.writeLong(stationDateTime);
        out.writeString(mTemperatureTendency);
        out.writeString(mSky);
        out.writeByte((byte) (mOPWM ? 1 : 0));
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<CurrentDataObject> CREATOR = new Parcelable.Creator<CurrentDataObject>() {
        public CurrentDataObject createFromParcel(Parcel in) {
            return new CurrentDataObject(in);
        }

        public CurrentDataObject[] newArray(int size) {
            return new CurrentDataObject[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private CurrentDataObject(Parcel in) {
        mStationID = in.readString();
        mStationName = in.readString();
        mTimeCreated = in.readDouble();
        mTempMax = in.readDouble();
        mTempMin = in.readDouble();
        mTempCurrent = in.readDouble();
        mPressure = in.readDouble();
        mHumidity = in.readInt();
        mWindSpeed = in.readDouble();
        mWindHighSpeed = in.readDouble();
        mWindDirection = in.readString();
        mSunset = in.readLong();
        mSunrise = in.readLong();
        mRain = in.readDouble();
        mSolarRadiation = in.readInt();
        mUVIndex = in.readDouble();
        stationDateTime = in.readLong();
        mTemperatureTendency = in.readString();
        mSky = in.readString();
        mOPWM = in.readByte() != 0;

        changeUnits = new ChangeUnits();
    }

}

