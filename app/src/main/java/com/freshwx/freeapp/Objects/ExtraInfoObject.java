package com.freshwx.freeapp.objects;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.freshwx.freeapp.R;


public class ExtraInfoObject implements Parcelable {

    private String mText;
    private String mIcon;
    private String mTimeCreated;
    private String mStationID;

    public ExtraInfoObject (String stationID, String text, String icon, String timeCreated){
        mStationID = stationID;
        mText = text;
        mIcon = icon;
        mTimeCreated = timeCreated;
    }

    public String getStationID() { return mStationID; }

    public void setStationID(String mText) { this.mStationID = mText; }


    public String getmText() { return mText; }

    public void setmText(String mText) { this.mText = mText; }


    public String getTimeCreated() { return mTimeCreated; }

    public void setTimeCreated(String mText) { this.mTimeCreated = mText; }


    public int getIcon(Context context) {
        String ubi;
        switch (mIcon) {
            case "low":
                ubi = "@drawable/bluethermometer";
                break;
            case "high":
                ubi = "@drawable/redthermometer";
                break;
            case "avg":
                ubi = "@drawable/greenthermometer";
                break;
            case "time":
                ubi = "@drawable/analogclock";
                break;
            case "rain":
                ubi = "@drawable/raincard";
                break;
            case "error":
                ubi = "@drawable/exclamationmark";
                break;
            default:
                ubi = "@drawable/bluethermometer";
        }
        return context.getResources().getIdentifier(ubi, null, context.getString(R.string.packageName));
    }

    public void setmIcon(String icon) { this.mIcon = icon; }



    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mStationID);
        out.writeString(mText);
        out.writeString(mIcon);
        out.writeString(mTimeCreated);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<ExtraInfoObject> CREATOR = new Parcelable.Creator<ExtraInfoObject>() {
        public ExtraInfoObject createFromParcel(Parcel in) {
            return new ExtraInfoObject(in);
        }

        public ExtraInfoObject[] newArray(int size) {
            return new ExtraInfoObject[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private ExtraInfoObject(Parcel in) {
        mStationID = in.readString();
        mText = in.readString();
        mIcon = in.readString();
        mTimeCreated = in.readString();
    }
}
