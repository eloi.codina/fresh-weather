package com.freshwx.freeapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.freshwx.freeapp.internet.FetchApiKey;
import com.freshwx.freeapp.summary.SummaryMain;


public class SplashActivity extends AppCompatActivity {
    Preferences preferences;
    int counter = 0;
    String uuid, mac;
    Thread tutorial, apiKey;
    boolean tutorialFinished = false, apiKeyFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new Preferences();

        tutorial = new Thread(new Runnable() {
            @Override
            public void run() {
                boolean isFirstStart = preferences.loadBoolean(getApplicationContext(), "isFirstRun");

                if (isFirstStart) {
                    //  Launch app intro
                    Intent i = new Intent(SplashActivity.this, Tutorial.class);
                    startActivityForResult(i, 0);
                } else tutorialFinished = true;
            }
        });
        apiKey = new Thread(new Runnable() {
            @Override
            public void run() {
                if (preferences.loadString(getApplicationContext(), "api_key", null) == null) {
                    uuid = preferences.getUUID(getApplicationContext());
                    mac = preferences.getWifiMacAddress();
                    connectToServer();
                } else {
                    apiKeyFinished = true;
                    startApp();
                }
            }
        });

        tutorial.start();
        apiKey.start();



    }

    public void startApp() {
        if (apiKeyFinished && tutorialFinished) {
            Intent intent = new Intent(this, SummaryMain.class);
            startActivity(intent);
            this.finish();
        } else {
            Thread countdown = new Thread(){
                @Override
                public void run(){
                    try {
                        synchronized(this){
                            wait(1000);
                            startApp();
                        }
                    }
                    catch(InterruptedException e){ e.printStackTrace();}
                }
            };
            countdown.start();
        }
    }

    public void connectToServer() {
        new FetchApiKey(getApplicationContext(), new FetchAPIListener(), uuid, mac).execute();
    }

    public void showError() {
        Toast.makeText(this, R.string.splash_error, Toast.LENGTH_LONG).show();
        finish();
    }

    public class FetchAPIListener implements AsyncTaskCompleteListener2<Boolean, String> {
        @Override
        public void onTaskComplete(Boolean successful, String key ) {
            counter += 1;
            if (successful) {
                preferences.saveStringSynchronized(getApplicationContext(), "api_key", key);
                apiKeyFinished = true;
                startApp();
            } else {
                if (counter <= 4) connectToServer();
                else showError();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) tutorialFinished = true;
    }

    public interface AsyncTaskCompleteListener2<T, Q> {
        void onTaskComplete(T result, Q result2);
    }
}