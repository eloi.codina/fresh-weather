package com.freshwx.freeapp;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class Preferences {

    public String loadString(Context context, String prefName, String defaultPreference){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(prefName, defaultPreference);
    }

    public void saveString(Context context, String preferenceName, String toSave){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(preferenceName, toSave);
        editor.apply();
    }

    public boolean saveStringSynchronized(Context context, String preferenceName, String toSave) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(preferenceName, toSave);
        return editor.commit();
    }

    public void saveBoolean(Context context, String preference, boolean condition) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(preference, condition);
        editor.apply();
    }

    public boolean loadBoolean(Context context, String prefName){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(prefName, true);
    }

    public void deleteAllPreferences(Context context) {
        String apiKey = loadString(context, "api_key", "");
        PreferenceManager.getDefaultSharedPreferences(context).edit().clear().apply();
        try {
            context.getSharedPreferences("localPreferences", 0).edit().clear().apply();
        } catch (Exception e) {
            //Nothing
        }
        try {
            context.getSharedPreferences("METEO_PREFS", 0).edit().clear().apply();
        } catch (Exception e){
            //Nothing
        }
        saveString(context, "api_key", apiKey);
    }

    public void updateLanguage(Context c) {
        String lang = loadString(c, "language", "");
        Configuration cfg = new Configuration();
        if (!TextUtils.isEmpty(lang))
            cfg.locale = new Locale(lang);
        else
            cfg.locale = Locale.getDefault();

        c.getResources().updateConfiguration(cfg, null);
    }

    public String getUUID(Context c) {
        String androidId = Settings.Secure.getString(c.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (androidId != null && !"9774d56d682e549c".equals(androidId) && androidId.length() >=2) {
            try {
                String id = Base64.encodeToString(androidId.getBytes(), Base64.URL_SAFE);
                id = id.replace("\r\n", "").replace("\n", "");
                return id;
            } catch (Exception e) { e.printStackTrace(); }
        }
        return "MQ==";
    }

    public String getWifiMacAddress() {
        try {
            String interfaceName = "wlan0";
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (!intf.getName().equalsIgnoreCase(interfaceName)){
                    continue;
                }

                byte[] mac = intf.getHardwareAddress();
                if (mac==null){
                    return "MQ==";
                }

                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) {
                    buf.append(String.format("%02X:", aMac));
                }
                if (buf.length()>0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                String id = Base64.encodeToString(buf.toString().getBytes(), Base64.URL_SAFE);
                id = id.replace("\r\n", "").replace("\n", "");
                return id;
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "MQ==";
    }
}
