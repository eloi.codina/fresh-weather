package com.freshwx.freeapp.search;

import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.internet.FetchCurrentData;
import com.freshwx.freeapp.internet.InternetConnection;
import com.freshwx.freeapp.objects.CurrentDataObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class FollowingFragment extends Fragment {

    ActivitySearch activity;

    String stationsList;

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    SwipeRefreshLayout swipeLayout;

    ProgressBar progressBar;
    TextView infoForUser;
    ImageButton tryAgain;
    FrameLayout frameLayout;

    ArrayList<CurrentDataObject> foundStations = new ArrayList<>();
    ArrayList<CurrentDataObject> mAdapterInformation = new ArrayList<>();

    final Preferences preferences = new Preferences();
    SearchTools searchTools;
    String apiKey;

    InternetConnection internet = new InternetConnection();

    public FollowingFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (ActivitySearch) getActivity();
        searchTools = new SearchTools(activity);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Bundle extras = getArguments();
        try {
            foundStations = extras.getParcelableArrayList("foundStations");
        } catch (Exception e) { //TODO
            }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.following_fragment, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.following_recycler_view);

        apiKey = preferences.loadString(activity, "api_key", "");

        swipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.following_fragment_swipe_refresh_layout);
        swipeLayout.setColorSchemeColors(ContextCompat.getColor(activity, R.color.accentColor));
        swipeLayout.setEnabled(false);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (internet.isConnected(activity)) {
                            startSearching();
                        } else
                            Toast.makeText(activity, getString(R.string.noConnection), Toast.LENGTH_LONG).show();
                        swipeLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        infoForUser = (TextView) v.findViewById(R.id.following_info_for_user);
        progressBar = (ProgressBar) v.findViewById(R.id.following_progressBar);
        tryAgain = (ImageButton) v.findViewById(R.id.following_try_again);
        frameLayout = (FrameLayout) v.findViewById(R.id.following_refreshing_layout);

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (internet.isConnected(activity)) {
                    tryAgain.setVisibility(View.GONE);
                    infoForUser.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    startSearching();
                }
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super .onViewCreated(view, savedInstanceState);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new FollowingRecyclerView(mAdapterInformation, activity);
        mRecyclerView.setAdapter(mAdapter);

        if (foundStations.size() == 0) {
            if (internet.isConnected(activity)) {
                startSearching();
            } else {
                progressBar.setVisibility(View.GONE);
                infoForUser.setText(getString(R.string.noConnection));
                infoForUser.setVisibility(View.VISIBLE);
                tryAgain.setVisibility(View.VISIBLE);
            }
        } else {
            setCards();
        }

        final LinearLayoutManager layoutManager = ((LinearLayoutManager)mRecyclerView.getLayoutManager());
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                if (firstVisiblePosition == 0) swipeLayout.setEnabled(true);
                else swipeLayout.setEnabled(false);
            }
        });

        ((FollowingRecyclerView) mAdapter).setOnItemClickListener(new FollowingRecyclerView.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                searchTools.showDialog(((FollowingRecyclerView) mAdapter).getDataset().get(position), position, mAdapter);
            }
        });
    }

    private void startSearching() {
        infoForUser.setVisibility(View.GONE);
        tryAgain.setVisibility(View.GONE);
        stationsList = getStationsList();
        if (stationsList != null) {
            foundStations.clear();
            new FetchCurrentData(activity, new FetchCurrentDataListener(), FetchCurrentData.FOLLOWING,
                    null, null, null, null, null, stationsList, apiKey).execute();
        } else {
            frameLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            infoForUser.setText(getString(R.string.stillNotFollowingAnyStation));
            infoForUser.setVisibility(View.VISIBLE);
            tryAgain.setVisibility(View.VISIBLE);
            swipeLayout.setEnabled(false);
        }
    }

    private String getStationsList(){
        String list = preferences.loadString(activity, "followedStations", null);
        String dataToPost = "";
        if (list == null || list.equals("") || list.equals("[]")) return null;
        try{
            JSONArray jsonArray = new JSONArray(list);

            for (int i = 0; i < jsonArray.length(); i++){
                dataToPost += jsonArray.get(i) + ",";
            }

            return dataToPost;


        } catch (JSONException e) {
            //e.printStackTrace();
        }

        return null;
    }

    private void setCards(){
        ((FollowingRecyclerView) mAdapter).clear();
        frameLayout.setVisibility(View.GONE);

        for (int i = 0; i < foundStations.size(); i++){
            ((FollowingRecyclerView) mAdapter).addItem(foundStations.get(i), i);
        }
        swipeLayout.setEnabled(true);
    }

    public class FetchCurrentDataListener implements AsyncTaskCompleteListener<ArrayList<CurrentDataObject>> {

        @Override
        public void onTaskComplete(ArrayList<CurrentDataObject> result) {
            foundStations = result;
            activity.foundStations = result;
            setCards();
        }
    }
}
