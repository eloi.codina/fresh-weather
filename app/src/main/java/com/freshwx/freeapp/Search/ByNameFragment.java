package com.freshwx.freeapp.search;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.internet.FetchCurrentData;
import com.freshwx.freeapp.internet.InternetConnection;
import com.freshwx.freeapp.objects.CurrentDataObject;

import java.util.ArrayList;

public class ByNameFragment extends Fragment {


    private Activity activity;

    ArrayList<CurrentDataObject> foundStations = new ArrayList<>();
    ArrayList<CurrentDataObject> mAdapterInformation = new ArrayList<>();

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    TextView informationForTheUser;

    ProgressBar progressBar;

    private String query;

    SearchTools searchTools;
    String apiKey;
    Preferences prefs = new Preferences();

    InternetConnection internet = new InternetConnection();

    private static final String tab00c0 = "AAAAAAACEEEEIIII" +
            "DNOOOOO\u00d7\u00d8UUUUYI\u00df" +
            "aaaaaaaceeeeiiii" +
            "\u00f0nooooo\u00f7\u00f8uuuuy\u00fey" +
            "AaAaAaCcCcCcCcDd" +
            "DdEeEeEeEeEeGgGg" +
            "GgGgHhHhIiIiIiIi" +
            "IiJjJjKkkLlLlLlL" +
            "lLlNnNnNnnNnOoOo" +
            "OoOoRrRrRrSsSsSs" +
            "SsTtTtTtUuUuUuUu" +
            "UuUuWwYyYZzZzZzF";


    public ByNameFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        searchTools = new SearchTools(activity);
        Bundle extras = getArguments();

        try {
            query = removeAccent(extras.getString("query"));
        } catch (Exception e) { query = ""; }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.by_name_fragment, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view_by_name);

        informationForTheUser = (TextView) v.findViewById(R.id.search_by_name_info_for_user);
        progressBar = (ProgressBar) v.findViewById(R.id.by_name_progress);

        apiKey = prefs.loadString(activity, "api_key", "");

        foundStations.clear();
        if (internet.isConnected(activity)) {
            startSearching(query);
        } else {
            informationForTheUser.setVisibility(View.VISIBLE);
            informationForTheUser.setText(getString(R.string.noConnection));
        }

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super .onViewCreated(view, savedInstanceState);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ByNameRecyclerView(mAdapterInformation, activity);
        mAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mAdapter);

        ((ByNameRecyclerView) mAdapter).setOnItemClickListener(new ByNameRecyclerView
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                searchTools.showDialog(((ByNameRecyclerView) mAdapter).getDataset().get(position), position, mAdapter);
            }
        });

    }

    private void setCards(){
        int size = foundStations.size();
        if (size > 0) {
            for (int i = 0; i < foundStations.size(); i++) {
                ((ByNameRecyclerView) mAdapter).addItem(foundStations.get(i), i);
            }
        } else {
            informationForTheUser.setVisibility(View.VISIBLE);
            informationForTheUser.setText(getString(R.string.noResults));
        }
    }

    private void startSearching(String text){
        informationForTheUser.setVisibility(View.INVISIBLE);

        if (text.equals("")){
            Toast.makeText(activity, getString(R.string.writeYourSearch), Toast.LENGTH_LONG).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            new FetchCurrentData(activity, new FetchCurrentDataListener(), FetchCurrentData.BYNAME,
                    null, null, null, null, text, null, apiKey).execute();
        }
    }

    public class FetchCurrentDataListener implements AsyncTaskCompleteListener<ArrayList<CurrentDataObject>> {

        @Override
        public void onTaskComplete(ArrayList<CurrentDataObject> result) {
            foundStations = result;
            setCards();
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Returns string without diacritics - 7 bit approximation.
     *
     * @param source string to convert
     * @return corresponding string without diacritics
     */
    public static String removeAccent(String source) {
        char[] vysl = new char[source.length()];
        char one;
        for (int i = 0; i < source.length(); i++) {
            one = source.charAt(i);
            if (one >= '\u00c0' && one <= '\u017f') {
                one = tab00c0.charAt((int) one - '\u00c0');
            }
            vysl[i] = one;
        }
        return new String(vysl);
    }

}
