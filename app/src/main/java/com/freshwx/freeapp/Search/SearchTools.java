package com.freshwx.freeapp.search;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;

import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.Tools;
import com.freshwx.freeapp.objects.CurrentDataObject;
import com.freshwx.freeapp.showStation.AboutStation;
import com.freshwx.freeapp.showStation.ClimateInformation;
import com.freshwx.freeapp.showStation.ShowData;


public class SearchTools {

    private static final int CURRENTDATA = 0;
    private static final int CLIMATEINFO = 1;
    private static final int STATIONINFO = 2;
    private static final int FOLLOW = 3;
    private static final int UNFOLLOW = 4;

    Activity activity;
    Tools tools;
    final Preferences preferences = new Preferences();

    public SearchTools(Activity activity) {
        this.activity = activity;
        tools = new Tools(activity);
    }

    public void showDialog(final CurrentDataObject dataObject, final int pos, final RecyclerView.Adapter adapter) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        CharSequence options[];
        final int descrOptions[];

        if (!dataObject.getOPWM()) {

            if (!tools.isFollowing(dataObject)) {
                options = new CharSequence[] {activity.getString(R.string.current_data), activity.getString(R.string.climateInformationTitle), activity.getString(R.string.about_station), activity.getString(R.string.follow)};
                descrOptions = new int[] {CURRENTDATA, CLIMATEINFO, STATIONINFO, FOLLOW};
            } else {
                options = new CharSequence[] {activity.getString(R.string.current_data), activity.getString(R.string.climateInformationTitle), activity.getString(R.string.about_station), activity.getString(R.string.unfollow)};
                descrOptions = new int[] {CURRENTDATA, CLIMATEINFO, STATIONINFO, UNFOLLOW};
            }

        } else {
            if (!tools.isFollowing(dataObject)) {
                options = new CharSequence[] {activity.getString(R.string.current_data), activity.getString(R.string.about_station), activity.getString(R.string.follow)};
                descrOptions = new int[] {CURRENTDATA, STATIONINFO, FOLLOW};
            } else {
                options = new CharSequence[] {activity.getString(R.string.current_data), activity.getString(R.string.about_station), activity.getString(R.string.unfollow)};
                descrOptions = new int[] {CURRENTDATA, STATIONINFO, UNFOLLOW};
            }
        }

        builder.setTitle(dataObject.getStationName());

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                switch (descrOptions[which]) {
                    case CURRENTDATA:
                        preferences.saveString(activity, "LAST_STATION", dataObject.getStationID());
                        preferences.saveBoolean(activity, "LAST_STATION_OPWM", dataObject.getOPWM());
                        intent.setClass(activity, ShowData.class);
                        intent.putExtra("currentData", dataObject);
                        activity.startActivity(intent);
                        break;
                    case CLIMATEINFO:
                        preferences.saveString(activity, "LAST_STATION", dataObject.getStationID());
                        preferences.saveBoolean(activity, "LAST_STATION_OPWM", dataObject.getOPWM());
                        intent.putExtra("currentData", dataObject);
                        intent.setClass(activity, ClimateInformation.class);
                        activity.startActivity(intent);
                        break;
                    case STATIONINFO:
                        if (dataObject.getOPWM())
                            showStationAboutDialog(dataObject.getStationName());
                        else {
                            preferences.saveString(activity, "LAST_STATION", dataObject.getStationID());
                            intent.setClass(activity, AboutStation.class);
                            activity.startActivity(intent);
                        }
                        break;
                    case FOLLOW:
                        if (tools.canFollow(dataObject))
                            tools.follow(dataObject);
                        break;
                    default:
                        ((FollowingRecyclerView) adapter).deleteItem(pos);
                        tools.unfollow(dataObject);
                        break;
                }

            }
        });
        builder.show();
    }

    private void showStationAboutDialog(String stationName) {
        new AlertDialog.Builder(activity)
                .setTitle(stationName)
                .setMessage(activity.getString(R.string.about_opwm))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).setNeutralButton(activity.getString(R.string.visitWebPage), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Uri uriUrl = Uri.parse("http://openweathermap.org/");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                activity.startActivity(launchBrowser);
            }
        }).show();
    }

}
