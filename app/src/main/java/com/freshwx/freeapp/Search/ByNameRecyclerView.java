package com.freshwx.freeapp.search;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.freshwx.freeapp.ChangeUnits;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.Tools;
import com.freshwx.freeapp.objects.CurrentDataObject;

import java.util.ArrayList;

public class ByNameRecyclerView extends RecyclerView.Adapter<ByNameRecyclerView.CurrentDataObjectHolder> {
    private ArrayList<CurrentDataObject> mDataset;
    private static MyClickListener myClickListener;

    ChangeUnits changeUnits;
    boolean celsius;
    Context mContext;

    ArrayList<Bitmap> bitmaps = new ArrayList<>();

    public static class CurrentDataObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        TextView city;
        TextView place;
        TextView distance;
        TextView temperature;
        TextView opwm;
        TextView pressure;
        TextView humidity;
        TextView wind;
        ImageView sky;
        TextView skyDescription;
        Button more;

        TextView temperatureUnits;


        public CurrentDataObjectHolder(View itemView) {
            super(itemView);
            city = (TextView) itemView.findViewById(R.id.search_city);
            place = (TextView) itemView.findViewById(R.id.search_stations_place);
            distance = (TextView) itemView.findViewById(R.id.search_stations_distance);
            temperature = (TextView) itemView.findViewById(R.id.search_temperature);
            opwm = (TextView) itemView.findViewById(R.id.search_stations_opwm);
            sky = (ImageView) itemView.findViewById(R.id.search_background);
            skyDescription = (TextView) itemView.findViewById(R.id.search_stations_sky_description);
            pressure = (TextView) itemView.findViewById(R.id.search_stations_pressure);
            humidity = (TextView) itemView.findViewById(R.id.search_stations_humidity);
            wind = (TextView) itemView.findViewById(R.id.search_stations_wind);
            more = (Button) itemView.findViewById(R.id.search_more);

            temperatureUnits = (TextView) itemView.findViewById(R.id.search_stations_units);

            more.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        ByNameRecyclerView.myClickListener = myClickListener;
    }

    public ByNameRecyclerView(ArrayList<CurrentDataObject> myDataset, Context context) {
        mDataset = myDataset;
        mContext = context;
        getUnitsSettings();
        changeUnits = new ChangeUnits();
        Tools tools = new Tools(context);

        bitmaps = tools.getPreviewBitmapsArray();
    }

    @Override
    public CurrentDataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_row_search, parent, false);

        return new CurrentDataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(CurrentDataObjectHolder holder, int position) {
        holder.city.setText(mDataset.get(position).getStationName());
        holder.temperature.setText(mDataset.get(position).getTempCurrentNoDecimals(mContext, false));
        holder.sky.setImageBitmap(bitmaps.get( mDataset.get(position).getPreviewBitmapPosition() ));
        holder.skyDescription.setText(mDataset.get(position).getSkyConditionText(mContext));
        holder.pressure.setText(mDataset.get(position).getPressure(mContext, true));
        holder.wind.setText(mDataset.get(position).getWindSpeed(mContext, true));
        holder.humidity.setText(mDataset.get(position).getHumidity(mContext, true));

        holder.place.setVisibility(View.INVISIBLE);
        holder.distance.setVisibility(View.INVISIBLE);

        if(!celsius) holder.temperatureUnits.setText(R.string.degreesF);
        else holder.temperatureUnits.setText(R.string.degreesC);

        if(mDataset.get(position).getOPWM()) holder.opwm.setVisibility(View.VISIBLE);
        else holder.opwm.setVisibility(View.INVISIBLE);


    }

    private void getUnitsSettings(){
        celsius = loadAppPreferences("temperatureUnits", "C").equals("C");
    }

    private String loadAppPreferences(String prefName, String defaultPreference){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        return prefs.getString(prefName, defaultPreference);
    }

    public void addItem(CurrentDataObject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    public ArrayList<CurrentDataObject> getDataset() {
        return mDataset;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) { return position; }

    public interface MyClickListener {
        void onItemClick(int position, View v);
    }
}