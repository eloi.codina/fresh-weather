package com.freshwx.freeapp.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.freshwx.freeapp.R;
import com.freshwx.freeapp.objects.CurrentDataObject;
import com.freshwx.freeapp.summary.SummaryMain;

import java.lang.reflect.Field;
import java.util.ArrayList;


public class ActivitySearch extends AppCompatActivity {
    private static final int FOLLOWING = 0;
    private static final int BYNAME = 1;
    int activeFragment = FOLLOWING;
    private EditText toolbarSearchView;
    ImageView searchClearButton;
    Toolbar toolbar;
    ArrayList<CurrentDataObject> foundStations = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        toolbarSearchView = (EditText) findViewById(R.id.search_view);
        searchClearButton = (ImageView) findViewById(R.id.search_clear);
        toolbar = (Toolbar) findViewById(R.id.search_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        // Setup search container view
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(toolbarSearchView, R.drawable.color_cursor);
        } catch (Exception ignored) { }

        toolbarSearchView.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE ||
                        event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    hideKeyboard();
                    replaceFragment(v.getText().toString());

                    return true; // consume.

                }
                return false; // pass on to other listeners.
            }
        });

        // Clear search text when clear button is tapped
        searchClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activeFragment == BYNAME) {
                    toolbarSearchView.setText("");
                    replaceFragment(null);
                }
            }
        });

        FollowingFragment following = new FollowingFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, following).commit();

    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = this.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(this);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void replaceFragment(@Nullable String query) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (query == null) {
            FollowingFragment following = new FollowingFragment();
            if (foundStations.size() != 0) {
                Bundle data = new Bundle();
                data.putParcelableArrayList("foundStations", foundStations);
                following.setArguments(data);
            }
            transaction.replace(R.id.fragment_container, following);
            activeFragment = FOLLOWING;
        } else {
            ByNameFragment search = new ByNameFragment();
            Bundle data = new Bundle();
            data.putString("query", query);
            search.setArguments(data);
            transaction.replace(R.id.fragment_container, search);
            activeFragment = BYNAME;
        }
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                Intent intent = new Intent(ActivitySearch.this, SummaryMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
