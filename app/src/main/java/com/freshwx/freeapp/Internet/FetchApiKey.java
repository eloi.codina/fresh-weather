package com.freshwx.freeapp.internet;


import android.content.Context;
import android.os.AsyncTask;

import com.freshwx.freeapp.R;
import com.freshwx.freeapp.SplashActivity;

import org.json.JSONObject;

public class FetchApiKey extends AsyncTask<String, String, JSONObject> {

    private Context context;
    private SplashActivity.AsyncTaskCompleteListener2<Boolean, String> listener;
    InternetConnection internet = new InternetConnection();
    String uuid, mac;

    public FetchApiKey(Context ctx, SplashActivity.AsyncTaskCompleteListener2<Boolean, String> listener, String uuid, String mac) {
        this.uuid = uuid;
        this.mac = mac;
        this.context = ctx;
        this.listener = listener;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        String internetParams = uuid + "/";
        internetParams += mac + "/?format=json";

        return internet.getJSONFromUrl(context.getString(R.string.apiKeyFetcherURL)+internetParams);
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        try {
            String api_key = json.getString("api_key");
            listener.onTaskComplete(true, api_key);
        } catch (Exception e) {

            e.printStackTrace();
            listener.onTaskComplete(false, "");
        }

    }

}

