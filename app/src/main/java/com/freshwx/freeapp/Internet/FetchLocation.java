package com.freshwx.freeapp.internet;

import android.content.Context;
import android.os.AsyncTask;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.objects.LocationObject;

import org.json.JSONObject;



public class FetchLocation extends AsyncTask<String, String, JSONObject> {
    private static final String TAG = "FetchMyDataTask";

    private AsyncTaskCompleteListener<LocationObject> listener;
    private double lat, lon;
    InternetConnection internet = new InternetConnection();
    Context context;
    String apiKey;

    public FetchLocation(AsyncTaskCompleteListener<LocationObject> listener, double lat, double lon, Context context, String apiKey) {
        this.listener = listener;
        this.lat = lat;
        this.lon = lon;
        this.context = context;
        this.apiKey = apiKey;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        String internetParams = Double.toString(lat)+"/";
        internetParams += Double.toString(lon)+"/";
        internetParams += "?format=json&key="+apiKey;
        return internet.getJSONFromUrl(context.getString(R.string.locationURL)+internetParams);
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        LocationObject location = new LocationObject(null, null, lat, lon, System.currentTimeMillis());
        try {
            JSONObject c = json.getJSONObject("data");
            location.setCity(c.getString("loc1"));
            location.setPlace(c.getString("loc2"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        listener.onTaskComplete(location);
    }
}
