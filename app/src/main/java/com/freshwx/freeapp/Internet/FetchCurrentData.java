package com.freshwx.freeapp.internet;


import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.objects.CurrentDataObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FetchCurrentData  extends AsyncTask<String, String, JSONObject> {

    public final static int NEARBY = 0;
    public final static int FOLLOWING = 1;
    public final static int BYNAME = 2;
    public final static int BYID = 3;

    private String lat, lon, stationID, query, stationsList, apiKey;
    private int fetchType;
    private Boolean opwm;
    private AsyncTaskCompleteListener<ArrayList<CurrentDataObject>> listener;
    ArrayList<CurrentDataObject> allStations = new ArrayList<>();
    Context context;
    InternetConnection internet = new InternetConnection();

    public FetchCurrentData(Context context, AsyncTaskCompleteListener<ArrayList<CurrentDataObject>> listener, int fetchType,
                            @Nullable String lat, @Nullable String lon, @Nullable String id, @Nullable Boolean opwm,
                            @Nullable String query_byName, @Nullable String stationsList, String apiKey) {
        this.context = context;
        this.listener = listener;
        this.fetchType = fetchType;
        this.lat = lat;
        this.lon = lon;
        this.stationID = id;
        this.opwm = opwm;
        this.query = query_byName;
        this.stationsList = stationsList;

        this.apiKey = apiKey;
    }

    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        String internetParams;
        String url;
        switch (fetchType) {
            case NEARBY:
                internetParams = lat + "/";
                internetParams += lon + "/?format=json&key="+apiKey;
                url = context.getString(R.string.currentDataNearbyURL)+internetParams;
                break;
            case BYID:
                internetParams = stationID + "/";
                internetParams += "opwm=" + (opwm ? "1" : "0") + "/?key="+apiKey+"&format=json";
                url = context.getString(R.string.currentDataByIDURL)+internetParams;
                break;
            case BYNAME:
                query = query.replace(" ", "%20");
                internetParams ="?format=json&q="+query+"&key="+apiKey;
                url = context.getString(R.string.currentDataByNameURL) + internetParams;
                break;
            default:
                internetParams = stationsList + "/?key="+apiKey+"&format=json";
                url = context.getString(R.string.currentDataFollowingURL) + internetParams;

        }
        return internet.getJSONFromUrl(url);
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        try {

            JSONArray receivedArray = json.getJSONArray("data");
            double timeCreated = System.currentTimeMillis();

            for (int i = 0; i < receivedArray.length(); i++) {
                JSONObject s = receivedArray.getJSONObject(i);

                String stationID = s.getString("stationid");
                String stationName = s.getString("station");
                long datetime = s.getLong("datetime");
                double tempMax = s.getDouble("tmax");
                double tempMin = s.getDouble("tmin");
                double tempCurrent = s.getDouble("temp");
                double pressure = s.getDouble("pres");
                int humidity = s.getInt("humt");
                double windSpeed = s.getDouble("wind");
                double windHighSpeed = s.getDouble("rwin");
                String windDirection = s.getString("dwin");
                long sunset = s.getLong("sset");
                long sunrise = s.getLong("sris");
                double rain = s.getLong("rain");
                int solarRadiation = s.getInt("solr");
                double uvIndex  = s.getDouble("uvin");
                String temperatureTendency = s.getString("tten");
                String sky = s.getString("sky");
                boolean opwm = s.getString("opwm").equals("true");


                CurrentDataObject tempObj = new CurrentDataObject(stationID, stationName,
                        timeCreated, tempMax, tempMin, tempCurrent, pressure, humidity, windSpeed,
                        windHighSpeed, windDirection, sunset, sunrise, rain, solarRadiation, uvIndex,
                        datetime, temperatureTendency, sky, opwm);
                allStations.add(tempObj);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        listener.onTaskComplete(allStations);

    }
}
