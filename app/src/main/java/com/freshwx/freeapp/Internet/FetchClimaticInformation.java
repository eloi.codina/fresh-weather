package com.freshwx.freeapp.internet;

import android.content.Context;
import android.os.AsyncTask;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.objects.ExtraInfoObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.StringTokenizer;


public class FetchClimaticInformation extends AsyncTask<String, String, JSONObject> {

    private Context context;
    private AsyncTaskCompleteListener<ArrayList<ExtraInfoObject>> listener;
    String currentStationID, apiKey;
    InternetConnection internet = new InternetConnection();

    public FetchClimaticInformation(Context ctx, AsyncTaskCompleteListener<ArrayList<ExtraInfoObject>> listener, String stationID, String apiKey) {
        this.context = ctx;
        this.listener = listener;
        this.currentStationID = stationID;
        this.apiKey = apiKey;

    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        String internetParams = context.getString(R.string.languageTag)+"/";
        internetParams += currentStationID+"/";
        internetParams += "?key="+apiKey+"&format=json";

        return internet.getJSONFromUrl(context.getString(R.string.extraInformationURL)+internetParams);
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        ArrayList<ExtraInfoObject> allInformationObject = new ArrayList<>();
        try {
            JSONObject c = json.getJSONObject("data");
            String mStationId = c.getString("stationid");
            String mInfo = c.getString(context.getString(R.string.languageTag));
            String mTimeCreated = Long.toString(System.currentTimeMillis());

            String[] allInfoFromInternet = mInfo.split("!!");

            for (String currentInformation : allInfoFromInternet) {
                StringTokenizer stringTokenizer = new StringTokenizer(currentInformation, "|");
                String icon = "low";
                String info = "ERROR";
                if (stringTokenizer.hasMoreTokens()) icon = stringTokenizer.nextToken();
                if (stringTokenizer.hasMoreTokens()) info = stringTokenizer.nextToken();

                ExtraInfoObject obj = new ExtraInfoObject(mStationId, info, icon, mTimeCreated);
                allInformationObject.add(obj);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        listener.onTaskComplete(allInformationObject);

    }
}
