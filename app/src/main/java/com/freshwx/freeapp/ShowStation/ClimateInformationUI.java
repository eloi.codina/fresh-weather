package com.freshwx.freeapp.showStation;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.internet.InternetConnection;
import com.freshwx.freeapp.objects.ExtraInfoObject;

import java.util.ArrayList;


public class ClimateInformationUI {

    Activity activity;
    Preferences preferences;
    InternetConnection internet;

    SwipeRefreshLayout swipeLayout;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    boolean celsius;
    ArrayList<ExtraInfoObject> infoObject = new ArrayList<>();

    private ArrayList<ExtraInfoObject> mAdapterInformation = new ArrayList<>();


    ClimateInformationUI(Activity activity) {
        this.activity = activity;
        preferences = new Preferences();
        internet = new InternetConnection();
        getViews();
    }
    
    private void getViews() {
        mRecyclerView = (RecyclerView) activity.findViewById(R.id.my_recycler_view);
        swipeLayout = (SwipeRefreshLayout) activity.findViewById(R.id.extra_information_swipe_refresh_layout);
        swipeLayout.setColorSchemeColors(ContextCompat.getColor(activity, R.color.accentColor));

        swipeLayout.setEnabled(false);

        celsius = preferences.loadString(activity, "temperatureUnits", "C").equals("C");

        //mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ClimateInformationRecyclerView(mAdapterInformation, !celsius, activity);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int totalScroll = 0;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalScroll += dy;
                if (totalScroll > 10) swipeLayout.setEnabled(false);
                else swipeLayout.setEnabled(true);
            }
        });

        ((ClimateInformationRecyclerView) mAdapter).setOnItemClickListener(new ClimateInformationRecyclerView
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                ((ClimateInformationRecyclerView) mAdapter).deleteItem(position);
                if (mAdapter.getItemCount() == 0) {
                    setUpdatedTexts(infoObject);
                }
            }
        });

    }

    public void setUpdatedTexts(ArrayList<ExtraInfoObject> allInformationObject){
        ((ClimateInformationRecyclerView) mAdapter).clear();
        infoObject = allInformationObject;
        for (int i = 0; i<allInformationObject.size(); i++)
            ((ClimateInformationRecyclerView) mAdapter).addItem(allInformationObject.get(i), i);


        swipeLayout.setEnabled(true);
    }
}
