package com.freshwx.freeapp.showStation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.freshwx.freeapp.CorrectTexts;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.objects.ExtraInfoObject;

import java.util.ArrayList;


public class ClimateInformationRecyclerView extends RecyclerView.Adapter<ClimateInformationRecyclerView.ExtraInfoObjectHolder> {
    private ArrayList<ExtraInfoObject> mDataset;
    private static MyClickListener myClickListener;
    CorrectTexts correctTexts;
    boolean mToFahrenheit;
    Context mContext;

    public void setOnItemClickListener(MyClickListener myClickListener) {
        ClimateInformationRecyclerView.myClickListener = myClickListener;
    }

    public ClimateInformationRecyclerView(ArrayList<ExtraInfoObject> myDataset, boolean toFahrenheit, Context context) {
        mDataset = myDataset;
        mToFahrenheit = toFahrenheit;
        correctTexts = new CorrectTexts();
        mContext = context;
    }

    @Override
    public ExtraInfoObjectHolder onCreateViewHolder(ViewGroup parent,
                                                    int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_row, parent, false);

        return new ExtraInfoObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(ExtraInfoObjectHolder holder, int position) {
        ExtraInfoObject tmp = mDataset.get(position);
        holder.label.setText(correctTexts.findReplaceTemperatures(tmp.getmText(), mToFahrenheit));

        holder.cardIcon.setImageResource(mDataset.get(position).getIcon(mContext));
    }

    public void addItem(ExtraInfoObject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);

    }

    public void deleteItem(int index) {
        try {
            mDataset.remove(index);
            notifyItemRemoved(index);
        } catch (ArrayIndexOutOfBoundsException e){ e.printStackTrace(); }
    }

    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);
    }





    public static class ExtraInfoObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        TextView label;
        ImageView cardIcon;

        public ExtraInfoObjectHolder(View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.textView);
            cardIcon = (ImageView) itemView.findViewById(R.id.iconcard);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }
}