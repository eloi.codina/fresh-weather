package com.freshwx.freeapp.showStation;


import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.freshwx.freeapp.ChangeUnits;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.UsersUnits;
import com.freshwx.freeapp.objects.CurrentDataObject;

public class ShowDataUI {

    Activity activity;
    ChangeUnits changeUnits;

    //Views where values are shown
    TextView temperature, summaryTemperature, maxTemperature, minTemperature, summaryMaxMin, pressure,
            summaryPressure, windSpeed, summaryWindSpeed, windHighSpeed, windDirection, humidity,
            summaryHumidity, rain, summaryRain, uvIndex, solarRadiation, sunset, sunrise, lastUpdate,
            skyText, dataFromOPWM, summaryTempUnits, arrowUp, arrowDown, locationText;


    ImageView tempTendency, sky, background;

    //Views to refresh
    SwipeRefreshLayout swipeLayout;
    ScrollView scrollView;

    RelativeLayout summaryLayoutTemperature, summaryLayoutOther, detailedSummary;

    UsersUnits usersUnits;

    boolean detailedSummaryOnScreen = false;
    boolean opwm;

    ShowDataUI(Activity activity, boolean opwm) {
        this.activity = activity;
        changeUnits = new ChangeUnits();
        usersUnits = new UsersUnits(activity);
        this.opwm = opwm;
        getViews();
    }

    private void getViews() {
        scrollView = (ScrollView) activity.findViewById(R.id.showDataScrollView);

        summaryTemperature = (TextView) activity.findViewById(R.id.summarytemp);
        summaryMaxMin = (TextView) activity.findViewById(R.id.summarymaxmin);
        summaryPressure = (TextView) activity.findViewById(R.id.summarypressure);
        summaryWindSpeed = (TextView) activity.findViewById(R.id.summarywind);
        summaryHumidity = (TextView) activity.findViewById(R.id.summaryhumidity);
        summaryRain = (TextView) activity.findViewById(R.id.summaryrain);

        temperature = (TextView) activity.findViewById(R.id.tempcurrent);
        maxTemperature = (TextView) activity.findViewById(R.id.tempmax);
        minTemperature = (TextView) activity.findViewById(R.id.tempmin);
        pressure = (TextView) activity.findViewById(R.id.pressure);
        windSpeed = (TextView) activity.findViewById(R.id.avgwind);
        windHighSpeed = (TextView) activity.findViewById(R.id.highestwind);
        windDirection = (TextView) activity.findViewById(R.id.directionwind);
        humidity = (TextView) activity.findViewById(R.id.humidity);
        rain = (TextView) activity.findViewById(R.id.rain);
        uvIndex = (TextView) activity.findViewById(R.id.uvindex);
        solarRadiation = (TextView) activity.findViewById(R.id.solrad);
        sunset = (TextView) activity.findViewById(R.id.sunset);
        sunrise = (TextView) activity.findViewById(R.id.sunrise);
        lastUpdate = (TextView) activity.findViewById(R.id.updated);
        sky = (ImageView) activity.findViewById(R.id.summaryskyimage);
        skyText = (TextView) activity.findViewById(R.id.summarysky);
        tempTendency = (ImageView) activity.findViewById(R.id.temperaturetendency);
        dataFromOPWM = (TextView) activity.findViewById(R.id.opwm);

        background = (ImageView) activity.findViewById(R.id.background_image_show_data);

        locationText = (TextView) activity.findViewById(R.id.location);

        //TextViews of units
        summaryTempUnits = (TextView) activity.findViewById(R.id.summarytempunits);

        arrowUp = (TextView) activity.findViewById(R.id.arrowup);
        arrowDown = (TextView) activity.findViewById(R.id.arrowdown);

        summaryLayoutOther = (RelativeLayout) activity.findViewById(R.id.summarylayoutother);
        summaryLayoutTemperature = (RelativeLayout) activity.findViewById(R.id.summarylayouttemperature);
        detailedSummary = (RelativeLayout) activity.findViewById(R.id.detailedsummary);

        swipeLayout = (SwipeRefreshLayout) activity.findViewById(R.id.show_data_swipe_refresh_layout);
        swipeLayout.setColorSchemeColors(ContextCompat.getColor(activity, R.color.accentColor));

        final GestureDetector gesture = new GestureDetector(activity,
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                           float velocityY) {

                        try {
                            if (e1.getY() > e2.getY() && !detailedSummaryOnScreen) {
                                showDetailedSummary();
                                swipeLayout.setEnabled(false);
                                detailedSummaryOnScreen = true;
                            } else if (e1.getY() < e2.getY()) {
                                hideDetailedSummary();
                                swipeLayout.setEnabled(true);
                                detailedSummaryOnScreen = false;
                            }
                        } catch (Exception e) { //TODO
                        }
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gesture.onTouchEvent(event);
            }
        });
    }

    private void showDetailedSummary() {
        int location[] = new int[2];
        summaryLayoutTemperature.getLocationOnScreen(location);
        final int yDelta = location[1];

        AlphaAnimation out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(200);
        summaryLayoutOther.startAnimation(out);
        summaryLayoutTemperature.startAnimation(out);
        if (opwm)
            dataFromOPWM.startAnimation(out);
        arrowDown.startAnimation(out);
        out.setFillAfter(true);

        out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { //TODO
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                summaryLayoutTemperature.setVisibility(View.GONE);
                summaryLayoutOther.setVisibility(View.GONE);
                arrowDown.setVisibility(View.GONE);
                dataFromOPWM.setVisibility(View.GONE);
                TranslateAnimation moveUp =
                        new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0,
                                Animation.RELATIVE_TO_SELF, 0,
                                Animation.ABSOLUTE, yDelta,
                                Animation.ABSOLUTE, 0);
                moveUp.setDuration(300);
                detailedSummary.setVisibility(View.VISIBLE);
                detailedSummary.startAnimation(moveUp);
                arrowUp.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) { //TODO
            }
        });
    }

    private void hideDetailedSummary() {
        int location[] = new int[2];
        detailedSummary.getLocationOnScreen(location);
        final int yDelta = location[1];

        TranslateAnimation moveDown =
                new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, 0,
                        Animation.ABSOLUTE, 0,
                        Animation.ABSOLUTE, yDelta-100);
        moveDown.setDuration(300);
        detailedSummary.startAnimation(moveDown);
        arrowUp.setVisibility(View.GONE);
        detailedSummary.setVisibility(View.GONE);

        moveDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { //TODO
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                AlphaAnimation in = new AlphaAnimation(0.0f, 1.0f);
                in.setDuration(200);
                summaryLayoutOther.startAnimation(in);
                summaryLayoutTemperature.startAnimation(in);
                if (opwm) {
                    dataFromOPWM.startAnimation(in);
                }
                arrowDown.startAnimation(in);
                in.setFillAfter(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) { //TODO
            }
        });
    }

    public void setUpdatedTexts(CurrentDataObject allDataToDisplay){
        summaryTempUnits.setText(usersUnits.temperatureUnits());

        summaryTemperature.setText(allDataToDisplay.getTempCurrent(activity, false));
        summaryMaxMin.setText(allDataToDisplay.getTempMin(activity, false) + " / " + allDataToDisplay.getTempMax(activity, false));
        summaryPressure.setText(" " + allDataToDisplay.getPressure(activity, true));
        summaryWindSpeed.setText(" " + allDataToDisplay.getWindSpeed(activity, true));
        summaryHumidity.setText(" " + allDataToDisplay.getHumidity(activity, true));
        summaryRain.setText(" " + allDataToDisplay.getRain(activity, true));

        temperature.setText(allDataToDisplay.getTempCurrent(activity, true));
        maxTemperature.setText(allDataToDisplay.getTempMax(activity, true));
        minTemperature.setText(allDataToDisplay.getTempMin(activity, true));
        pressure.setText(allDataToDisplay.getPressure(activity, true));
        windSpeed.setText(allDataToDisplay.getWindSpeed(activity, true));
        windHighSpeed.setText(allDataToDisplay.getWindHighSpeed(activity, true));
        windDirection.setText(allDataToDisplay.getWindDirection(activity));
        humidity.setText(allDataToDisplay.getHumidity(activity, true));
        rain.setText(allDataToDisplay.getRain(activity, true));
        solarRadiation.setText(Html.fromHtml(allDataToDisplay.getSolarRadiation(activity, true)));
        uvIndex.setText(allDataToDisplay.getUVIndex(activity));
        sunset.setText(allDataToDisplay.getSunset());
        sunrise.setText(allDataToDisplay.getSunrise());
        lastUpdate.setText(activity.getString(R.string.updatedAt) + " " +
                allDataToDisplay.getDateStation(activity) + " " + activity.getString(R.string.at) + " " +
                allDataToDisplay.getTimeStation(activity) + " " + activity.getString(R.string.UTC));

        if (!opwm) {
            tempTendency.setImageDrawable(ContextCompat.getDrawable(activity, allDataToDisplay.getTemperatureTendency(activity)));
        } else {
            tempTendency.setVisibility(View.GONE);
            summaryTemperature.setPadding(35, 0, 0, 0);
        }

        sky.setImageDrawable(ContextCompat.getDrawable(activity, allDataToDisplay.getWhiteSkyIcon(activity)));
        skyText.setText(allDataToDisplay.getSkyConditionText(activity));
        background.setImageDrawable(allDataToDisplay.getBackgroundDrawable(activity));

        locationText.setText(allDataToDisplay.getStationName());
    }
}
