package com.freshwx.freeapp.showStation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.internet.FetchCurrentData;
import com.freshwx.freeapp.internet.InternetConnection;
import com.freshwx.freeapp.objects.CurrentDataObject;
import com.freshwx.freeapp.objects.ExtraInfoObject;

import java.util.ArrayList;


public class ShowData extends AppCompatActivity {

    CurrentDataObject dataObject;
    ArrayList<ExtraInfoObject> infoObject;

    InternetConnection internet;
    ShowDataUI s;

    Toolbar toolbar;
    SwipeRefreshLayout refreshLayout;

    String apiKey;
    Preferences prefs = new Preferences();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_data);

        toolbar = (Toolbar) findViewById(R.id.tool_bar_data);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.show_data_swipe_refresh_layout);
        refreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.accentColor));

        apiKey = prefs.loadString(getApplicationContext(), "api_key", "");

        internet = new InternetConnection();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (internet.isConnected(getApplicationContext()))
                            new FetchCurrentData(getApplicationContext(), new FetchCurrentDataListener(), FetchCurrentData.BYID,
                                    null, null, dataObject.getStationID(), dataObject.getOPWM(), null, null, apiKey).execute();
                        else
                            Toast.makeText(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG).show();
                        refreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });

        Intent intent = getIntent();
        dataObject = intent.getParcelableExtra("currentData");
        infoObject = intent.getParcelableArrayListExtra("climateInfo");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        s = new ShowDataUI(this, dataObject.getOPWM());
        s.setUpdatedTexts(dataObject);

        toolbar.setTitle(R.string.current_data_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent resultIntent = new Intent();
                resultIntent.putExtra("currentData", dataObject);
                resultIntent.putParcelableArrayListExtra("climateInfo", infoObject);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }





    public class FetchCurrentDataListener implements AsyncTaskCompleteListener<ArrayList<CurrentDataObject>> {

        @Override
        public void onTaskComplete(ArrayList<CurrentDataObject> result) {
            if (result.size() >= 1) {
                dataObject = result.get(0);
                s.setUpdatedTexts(dataObject);

            } else {
                refreshLayout.setRefreshing(false);
            }
        }
    }

}
