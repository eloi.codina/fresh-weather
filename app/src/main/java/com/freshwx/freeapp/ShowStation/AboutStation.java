package com.freshwx.freeapp.showStation;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.freshwx.freeapp.ChangeUnits;
import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.internet.InternetConnection;

import org.json.JSONObject;


public class AboutStation extends AppCompatActivity {
    private TextView cityView, latitudeView, longitudeView, amslView, webPageView, stationTypeView,
            extraInformationView, extraInformationTitleView, amslUnits;

    private FrameLayout frameLayout;
    private ProgressBar progressBar;
    private TextView infoForUser;
    private ImageButton tryAgain;

    private String stat, lat, lon, amsl, modl, wbpg, info;

    private final InternetConnection internet = new InternetConnection();

    private final Preferences preferences = new Preferences();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.station_about);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        frameLayout = (FrameLayout) findViewById(R.id.about_refreshing_layout);
        progressBar = (ProgressBar) findViewById(R.id.about_progressBar);
        infoForUser = (TextView) findViewById(R.id.about_info_for_user);
        tryAgain = (ImageButton) findViewById(R.id.about_try_again);

        cityView = (TextView) findViewById(R.id.about_city);
        latitudeView = (TextView) findViewById(R.id.about_latitude);
        longitudeView = (TextView) findViewById(R.id.about_longitude);
        amslView = (TextView) findViewById(R.id.about_amsl);
        webPageView = (TextView) findViewById(R.id.about_web_page);
        stationTypeView = (TextView) findViewById(R.id.about_station_model);
        extraInformationView = (TextView) findViewById(R.id.about_extra);
        extraInformationTitleView = (TextView) findViewById(R.id.about_extra_title);

        amslUnits = (TextView) findViewById(R.id.about_amsl_units);

        webPageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(wbpg));
                startActivity(i);
            }
        });

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(internet.isConnected(getApplicationContext())){
                    progressBar.setVisibility(View.VISIBLE);
                    infoForUser.setVisibility(View.GONE);
                    tryAgain.setVisibility(View.GONE);
                    new GetStationInformation().execute();
                }
            }
        });

        if(internet.isConnected(this)) {
            new GetStationInformation().execute();
        } else {
            frameLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            infoForUser.setText(getString(R.string.noConnection));
            infoForUser.setVisibility(View.VISIBLE);
            tryAgain.setVisibility(View.VISIBLE);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_about_station);
        toolbar.setTitle(R.string.about_station);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                AboutStation.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTexts() {
        ChangeUnits changeUnits = new ChangeUnits();
        boolean metric = preferences.loadString(this, "distanceUnits", "met").equals("met");

        cityView.setText(stat);
        latitudeView.setText(lat);
        longitudeView.setText(lon);
        amslView.setText(changeUnits.mToYd(amsl, !metric));
        stationTypeView.setText(modl);

        if(!info.equals("NON")) {
            extraInformationView.setText(info);
        }
        else{
            extraInformationView.setVisibility(View.INVISIBLE);
            extraInformationTitleView.setVisibility(View.INVISIBLE);
        }

        if (!wbpg.equals("NON")) {
            webPageView.setClickable(true);
            webPageView.setText(getString(R.string.visitWebPage));
            webPageView.setTextColor(ContextCompat.getColor(this, R.color.accentColor));
        }
        else {
            webPageView.setClickable(false);
            webPageView.setTextColor(ContextCompat.getColor(this, R.color.black));
            webPageView.setText(getString(R.string.noWebPageAvailable));
        }

        if(!metric) amslUnits.setText(getString(R.string.yd));

        frameLayout.setVisibility(View.GONE);
    }



    private class GetStationInformation extends AsyncTask<String, String, JSONObject> {
        String apiKey;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            apiKey = preferences.loadString(getApplicationContext(), "api_key", null);
            if (apiKey == null) apiKey = "";
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            String internetParams = preferences.loadString(getApplicationContext(), "LAST_STATION", "") + "/?format=json&key=";
            internetParams += apiKey;
            return internet.getJSONFromUrl(getString(R.string.aboutURL) + internetParams);
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                JSONObject c = json.getJSONObject("data");

                stat = c.getString("station");
                lat = getDMS(c.getDouble("lat"), "lat");
                lon = getDMS(c.getDouble("lon"), "lon");
                amsl = c.getString("amsl");
                modl = c.getString("model");
                wbpg = c.getString("webpage");
                info = c.getString("othinfo");

                setTexts();


            } catch (Exception e) {             //I want to handle ALL errors
                e.printStackTrace();
                infoForUser.setText(getString(R.string.tryLater));
                progressBar.setVisibility(View.GONE);
                infoForUser.setVisibility(View.VISIBLE);
                tryAgain.setVisibility(View.VISIBLE);
            }
        }

        private String getDMS(double degree, String type){
            double decimalDegree = Math.abs(degree);
            int degrees = (int) decimalDegree;
            double decimals = decimalDegree - (double) degrees;
            int minutes =(int) (decimals*60);
            int seconds = (int) ((decimals - ((double) minutes)/60)*3600);

            switch (type){
                case "lat":
                    if (degree>0) {
                        return (Integer.toString(degrees) + "\u00B0" + Integer.toString(minutes) + "'"
                                + Integer.toString(seconds) + "\"N");
                    } else {
                        return (Integer.toString(degrees) + "\u00B0" + Integer.toString(minutes) + "'"
                                + Integer.toString(seconds) + "\"S");
                    }
                case "lon":
                    if (degree>0) {
                        return (Integer.toString(degrees) + "\u00B0" + Integer.toString(minutes) + "'"
                                + Integer.toString(seconds) + "\"E");
                    } else {
                        return (Integer.toString(degrees) + "\u00B0" + Integer.toString(minutes) + "'"
                                + Integer.toString(seconds) + "\"W");
                    }
            }

            return (Integer.toString(degrees) + "\u00B0" + Integer.toString(minutes) + "'"
                    + Integer.toString(seconds) + "\"");
        }
    }
}