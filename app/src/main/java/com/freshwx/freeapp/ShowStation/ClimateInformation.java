package com.freshwx.freeapp.showStation;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.internet.FetchClimaticInformation;
import com.freshwx.freeapp.internet.InternetConnection;
import com.freshwx.freeapp.objects.CurrentDataObject;
import com.freshwx.freeapp.objects.ExtraInfoObject;

import java.util.ArrayList;

public class ClimateInformation extends AppCompatActivity {

    CurrentDataObject dataObject;
    ArrayList<ExtraInfoObject> infoObject;
    ClimateInformationUI ui;
    Toolbar toolbar;
    Preferences preferences = new Preferences();
    String apiKey;

    InternetConnection internet;

    SwipeRefreshLayout swipeLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.extra_information);

        toolbar = (Toolbar) findViewById(R.id.tool_bar_climate);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.extra_information_swipe_refresh_layout);
        swipeLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.accentColor));
        swipeLayout.setEnabled(false);

        apiKey = preferences.loadString(getApplicationContext(), "api_key", "");

        Intent intent = getIntent();
        dataObject = intent.getParcelableExtra("currentData");
        infoObject = intent.getParcelableArrayListExtra("climateInfo");

        if (infoObject == null) infoObject = new ArrayList<>();

        ui = new ClimateInformationUI(this);
        internet = new InternetConnection();

        if (infoObject.size() == 0 && dataObject != null)
            new FetchClimaticInformation(getApplicationContext(), new FetchClimaticInfoListener(), dataObject.getStationID(), apiKey).execute();
        else ui.setUpdatedTexts(infoObject);


        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (internet.isConnected(getApplicationContext())) {
                            new FetchClimaticInformation(getApplicationContext(),
                                    new FetchClimaticInfoListener(), dataObject.getStationID(), apiKey).execute();
                        } else Toast.makeText(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG).show();
                        swipeLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });

        toolbar.setTitle(R.string.climateInformationTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent resultIntent = new Intent();
                resultIntent.putExtra("currentData", dataObject);
                resultIntent.putParcelableArrayListExtra("climateInfo", infoObject);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }





    public class FetchClimaticInfoListener implements AsyncTaskCompleteListener<ArrayList<ExtraInfoObject>> {

        @Override
        public void onTaskComplete(ArrayList<ExtraInfoObject> result) {
            infoObject.clear();
            infoObject = result;
            if (result.size() != 0) { ui.setUpdatedTexts(infoObject); }
        }
    }
}
