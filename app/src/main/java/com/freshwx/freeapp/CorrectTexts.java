package com.freshwx.freeapp;

public class CorrectTexts {

    public String findReplaceTemperatures(String text, boolean toFahrenheit){
        int opening = 0;
        int closing = 0;
        ChangeUnits changeUnits = new ChangeUnits();

        if(toFahrenheit) text = text.replace("(C)", "\u00B0F");
        else text = text.replace("(C)", "\u00B0C");

        while(true){
            opening = nextIndex(text, '<', opening+1);
            closing = nextIndex(text, '>', closing+1);

            if(opening == -1 || closing == -1) break;
            else {
                String temperature = text.substring(opening + 1, closing);
                if(toFahrenheit) temperature = changeUnits.celsiusToFahrenheit(temperature, true);
                else temperature = changeUnits.celsiusToFahrenheit(temperature, false);
                text = text.replace( text.substring(opening,closing+1), temperature );
            }
        }

        return text;
    }

    private static int nextIndex(String text, char character, int start){
        return text.indexOf(character, start);
    }

}
