package com.freshwx.freeapp.helpSettings;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.freshwx.freeapp.R;
import com.freshwx.freeapp.Tutorial;
import com.suredigit.inappfeedback.FeedbackDialog;
import com.suredigit.inappfeedback.FeedbackSettings;


public class HelpActivity extends AppCompatActivity {

    TableRow tour, website,feedback, imageLicense, privacyPolicy, tweet;
    Toolbar toolbar;

    private FeedbackDialog feedBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_and_feedback);

        toolbar = (Toolbar) findViewById(R.id.toolbar_help);
        tour = (TableRow) findViewById(R.id.tour_row);
        website = (TableRow) findViewById(R.id.website_row);
        feedback = (TableRow) findViewById(R.id.feedback_row);
        imageLicense = (TableRow) findViewById(R.id.image_license_row);
        privacyPolicy = (TableRow) findViewById(R.id.privacy_row);
        tweet = (TableRow) findViewById(R.id.tweet_row);

        toolbar.setTitle(getString(R.string.aboutTitle));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setOnClickListeners();

        setUpFeedback();

    }

    @Override
    protected void onPause() {
        super.onPause();
        feedBack.dismiss();
    }

    private void setUpFeedback() {
        FeedbackSettings feedbackSettings = new FeedbackSettings();

        //SUBMIT-CANCEL BUTTONS
        feedbackSettings.setCancelButtonText(getString(R.string.feedback_no));
        feedbackSettings.setSendButtonText(getString(R.string.feedback_send));

        //DIALOG TEXT
        feedbackSettings.setText(getString(R.string.feedback_text_help));
        feedbackSettings.setYourComments(getString(R.string.feedback_type_here));
        feedbackSettings.setTitle(getString(R.string.feedback_dialog_title));

        //TOAST MESSAGE
        feedbackSettings.setToast(getString(R.string.feedback_message_sent));
        feedbackSettings.setToastDuration(Toast.LENGTH_LONG);

        //RADIO BUTTONS
        feedbackSettings.setRadioButtons(true); // Disables radio buttons
        feedbackSettings.setBugLabel(getString(R.string.feedback_bug));
        feedbackSettings.setIdeaLabel(getString(R.string.feedback_idea));
        feedbackSettings.setQuestionLabel(getString(R.string.feedback_question));

        //RADIO BUTTONS ORIENTATION AND GRAVITY
        feedbackSettings.setOrientation(LinearLayout.VERTICAL);
        feedbackSettings.setGravity(Gravity.LEFT);

        //SET DIALOG MODAL
        feedbackSettings.setModal(true); //Default is false

        //DEVELOPER REPLIES
        feedbackSettings.setReplyTitle(getString(R.string.feedback_message_developer));
        feedbackSettings.setReplyCloseButtonText(getString(R.string.feedback_close));
        feedbackSettings.setReplyRateButtonText(getString(R.string.feedback_rate));

        //DEVELOPER CUSTOM MESSAGE (NOT SEEN BY THE END USER)
        feedbackSettings.setDeveloperMessage(getString(R.string.feedback_developer_custom_message));

        feedBack = new FeedbackDialog(this, "AF-57C7549A3CAB-51", feedbackSettings);
    }

    private void setOnClickListeners() {
        tour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HelpActivity.this, Tutorial.class);
                startActivity(i);
            }
        });

        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriUrl = Uri.parse("https://freshwx.com/");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedBack.show();
            }
        });

        imageLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageLicenseDialog();
            }
        });

        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriUrl = Uri.parse("https://freshwx.com/privacypolicy");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        tweet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("twitter://user?screen_name=freshweather"));
                    startActivity(intent);

                }catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://twitter.com/freshweather")));
                }
            }
        });
    }

    private void showImageLicenseDialog() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.image_license_title))
                .setMessage(getString(R.string.image_license_text))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

}
