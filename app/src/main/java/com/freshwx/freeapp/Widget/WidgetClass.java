package com.freshwx.freeapp.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.IBinder;
import android.widget.RemoteViews;

import com.freshwx.freeapp.AsyncTaskCompleteListener;
import com.freshwx.freeapp.Preferences;
import com.freshwx.freeapp.R;
import com.freshwx.freeapp.UsersUnits;
import com.freshwx.freeapp.internet.FetchCurrentData;
import com.freshwx.freeapp.internet.FetchLocation;
import com.freshwx.freeapp.location.LocationUpdater;
import com.freshwx.freeapp.location.LocationUpdaterInterface;
import com.freshwx.freeapp.objects.CurrentDataObject;
import com.freshwx.freeapp.objects.LocationObject;
import com.freshwx.freeapp.summary.SummaryMain;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;


public class WidgetClass extends AppWidgetProvider implements LocationUpdaterInterface {

    Context mContext;

    boolean  locationUpdaterBound;
    CurrentDataObject data;
    LocationObject locationObject;
    UsersUnits usersUnits;

    RemoteViews remoteViews;

    LocationUpdater locationUpdater;
    Preferences prefs = new Preferences();

    String apiKey;

    @Override
    public void onEnabled(Context context) {
        mContext = context;
        usersUnits = new UsersUnits(context);
        apiKey = prefs.loadString(context.getApplicationContext(), "api_key", "");
        search();
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        mContext = context;
        apiKey = prefs.loadString(context.getApplicationContext(), "api_key", "");

        remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        usersUnits = new UsersUnits(context);

        search();

        Intent intent = new Intent(context, WidgetClass.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.widget_refresh, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetIds[0], remoteViews);

    }


    private void updateWidgetTexts(){

        remoteViews.setTextViewText(R.id.search_city, locationObject.getCity());
        remoteViews.setTextViewText(R.id.widget_place, locationObject.getPlace());

        remoteViews.setTextViewText(R.id.widget_humidity, data.getHumidity(mContext, true));
        remoteViews.setTextViewText(R.id.widget_pressure, data.getPressure(mContext, true));
        remoteViews.setTextViewText(R.id.widget_sky_description, data.getSkyConditionText(mContext));
        remoteViews.setTextViewText(R.id.search_temperature, data.getTempCurrentNoDecimals(mContext, false));
        remoteViews.setTextViewText(R.id.widget_temperature_units, usersUnits.temperatureUnits());
        remoteViews.setTextViewText(R.id.widget_wind, data.getWindSpeed(mContext, true));

        remoteViews.setImageViewResource(R.id.widget_sky, data.getColorSkyIcon(mContext));

        Intent intent = new Intent(mContext, SummaryMain.class);
        intent.putExtra("location", locationObject);
        intent.putExtra("currentData", data);
        PendingIntent pi = PendingIntent.getActivity(mContext, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.widget_full_report, pi);

        ComponentName componentName = new ComponentName(mContext, WidgetClass.class);
        AppWidgetManager.getInstance(mContext).updateAppWidget(componentName, remoteViews);

    }

    public void search() {
        Intent intent = new Intent(mContext, LocationUpdater.class);
        mContext.getApplicationContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    /* LOCATION */

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // cast the IBinder and get MyService instance
            LocationUpdater.LocationBinder binder = (LocationUpdater.LocationBinder) service;
            locationUpdater = binder.getService();
            locationUpdater.setCallbacks(WidgetClass.this); // register
            locationUpdaterBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            locationUpdaterBound = false;
        }
    };

    /* Defined by ServiceCallbacks interface */
    @Override
    public void locationUpdated(Location location) {
        new FetchLocation(new FetchLocationListener(), location.getLatitude(), location.getLongitude(), mContext, apiKey).execute();
        disconnectFromLocationService();
    }

    @Override
    public void enableLocation(Status status) {
        disconnectFromLocationService();
    }

    private void disconnectFromLocationService() {
        if (locationUpdaterBound) {
            locationUpdater.setCallbacks(null); // unregister
            mContext.unbindService(serviceConnection);
            locationUpdaterBound = false;
        }
    }







    public class FetchLocationListener implements AsyncTaskCompleteListener<LocationObject> {

        @Override
        public void onTaskComplete(LocationObject result) {
            locationObject = result;
            new FetchCurrentData(mContext, new FetchCurrentDataListener(), FetchCurrentData.NEARBY,
                    Double.toString(locationObject.getLat()), Double.toString(locationObject.getLon()),
                    null, null, null, null, apiKey).execute();
        }
    }

    public class FetchCurrentDataListener implements AsyncTaskCompleteListener<ArrayList<CurrentDataObject>> {

        @Override
        public void onTaskComplete(ArrayList<CurrentDataObject> result) {
            if (result.size() >= 1) {
                data = result.get(0);
                updateWidgetTexts();
            }
        }
    }
}
