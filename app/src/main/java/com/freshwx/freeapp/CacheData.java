package com.freshwx.freeapp;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/*
 *
 * This class is not used anymore!
 *
 */
public class CacheData {

    CacheData () {}

    public static void writeObject(Context context, String key, Object object) throws IOException {
        File myCacheFile = new File(context.getCacheDir().getPath() + File.separator + key);
        FileOutputStream fos = new FileOutputStream(myCacheFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
        fos.close();
    }

    public static Object readObject(Context context, String key) throws IOException, ClassNotFoundException {
        File myCacheFile = new File(context.getCacheDir().getPath() + File.separator + key);
        FileInputStream fis = new FileInputStream(myCacheFile);
        ObjectInputStream ois = new ObjectInputStream(fis);
        return ois.readObject();
    }
}
